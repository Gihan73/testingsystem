<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/category_view', 'HomeController@category')->name('category_view');

//student login
// Route::post('/login_submit','StudentRegistrationController@store')->name('login');
//Student Registration
Route::get('/stu_user', 'StudentRegistrationController@index')->name('stu_user_data');
Route::get('/stu_user_create', 'StudentRegistrationController@create')->name('stu_user_form');
Route::post('/stu_user_store', 'StudentRegistrationController@store')->name('store_stu_reg');

//Lecturer Registration
Route::get('/LEC_user', 'UserRegistrationController@index')->name('LEC_user_data');
Route::get('/LEC_user_create', 'UserRegistrationController@create')->name('LEC_user_form');
Route::post('/LEC_user_store', 'UserRegistrationController@store')->name('LEC_store_reg');

//ED Registration
Route::get('/ED_user', 'UserRegistrationController@index')->name('ED_user_data');
Route::get('/ED_user_create', 'UserRegistrationController@create')->name('ED_user_form');
Route::post('/ED_user_store', 'UserRegistrationController@store')->name('ED_store_reg');

//Courses
Route::get('/coursesForFaculty/{department_code}', 'CourseController@courseForFaculty')->name('courses_for_faculty');

//Profile
Route::get('/profile', 'HomeController@profile')->name('profile');

//Modules
Route::get('/module_result', 'ModuleController@index')->name('module_result');
Route::get('/module_create', 'ModuleController@create')->name('module_create');
Route::post('/module_store', 'ModuleController@store')->name('module_store');


//Module to enroll
Route::get('/module_to_enroll_result', 'ModuleToEnrollController@index')->name('module_to_enroll_result');
Route::get('/module_create_to_enroll', 'ModuleToEnrollController@create')->name('module_create_to_enroll');
Route::post('/module_enroll_store', 'ModuleToEnrollController@store')->name('module_enroll_store');
Route::get('/enroll_my_subject', 'ModuleToEnrollController@enrollMySubjects')->name('enroll_my_subject');
Route::get('/getall_subject', 'ModuleToEnrollController@getAllSubjects')->name('getall_subject');
Route::get('/enroll_subject/{enroll_id}', 'ModuleToEnrollController@enrollSubjects')->name('enroll_subjects');
Route::get('/unenroll_subject/{enroll_id}', 'ModuleToEnrollController@unEnrollSubjects')->name('unenroll_subjects');
Route::get('/submit_subjects', 'ModuleToEnrollController@submitSubjects')->name('submit_subjects');
Route::get('/modules_for_course/{department}/{course_id}', 'ModuleToEnrollController@moduleForCourse')->name('modules_for_course');
// Route::get('/modules_for_course/{department}/{course_id}', 'ModuleToEnrollController@moduleForCourse')->name('modules_for_course');

//Upload Marksheets
Route::get('/upload_form', 'MarksheetUploadController@marksheetForm')->name('upload_form');
Route::post('/store_mark_sheet', 'MarksheetUploadController@storeMarksheet')->name('store_mark_sheet');
Route::get('/pnding_mrksht_list' , 'MarksheetUploadController@pendingMarksheetList')->name('pnding_mrksht_list');
Route::get('/read_n_approve/{file_name}/{semester_yr}' , 'MarksheetUploadController@readNApproveMarks')->name('read_n_approve');


//Action pool
Route::get('/action_pool', function () {
    return view('ed.action_pool');
});
Route::get('/generate_final_mark/{year}' , 'CalculationsController@generateFinalMarks')->name('generate_final_mark');
Route::get('/semester_gpa_calculate' , 'CalculationsController@semesterGPACalculate')->name('semester_gpa_calculate');
Route::get('/overall_gpa_calculate' , 'CalculationsController@overallGPACalculate')->name('overall_gpa_calculate');


//Repeat Process
Route::get('/repeat_page' , 'RepeatController@index')->name('repeat_page');
Route::get('/repeat_list' , 'RepeatController@getRepeatList')->name('repeat_list');
Route::get('/enroll_repeat_subject/{enroll_id}', 'RepeatController@enrollRepeatSubjects')->name('enroll_repeat_subject');
Route::get('/unenroll_repeat_subject/{enroll_id}', 'RepeatController@unenrollRepeatSubjects')->name('unenroll_repeat_subject');
Route::get('/confirm_form', 'RepeatController@confirmRepeatSubjects')->name('confirm_form');
Route::post('/submit_repeat', 'RepeatController@submitRepeatSubjects')->name('submit_repeat');
Route::get('/pnding_repeat_list', 'RepeatController@pendingRepeatList')->name('pnding_repeat_list');
Route::get('/rpt_application_vw/{app_id}', 'RepeatController@viewRepeatApplication')->name('rpt_application_vw');
Route::post('/approve_rpt_frm/{app_id}', 'RepeatController@approveRepeatApplication')->name('approve_rpt_frm');

//attitude test mark section
Route::get('/attitudeTestView' , 'AttitudeTest@index')->name('attitudeTestView');
Route::post('/store_attituteTest_marks', 'AttitudeTest@storeAttituteTestMarks')->name('store_attituteTest_marks');