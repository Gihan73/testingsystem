-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2020 at 07:58 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `apptitute_mark`
--

CREATE TABLE `apptitute_mark` (
  `apptitute_row_no` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_year` int(11) NOT NULL,
  `module_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `continuous_assignment_mark`
--

CREATE TABLE `continuous_assignment_mark` (
  `rowno` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark_rowno` bigint(10) UNSIGNED NOT NULL,
  `current_date` int(11) NOT NULL,
  `assignment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assignment_mark` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `course_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL COMMENT 'no of semesters',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_code`, `course_name`, `department_code`, `duration`, `created_at`, `updated_at`) VALUES
('CS0001', 'Earth Resource Engineering', 'DP0001', 3, NULL, NULL),
('CS0002', 'Electrical Engineering', 'DP0001', 6, NULL, NULL),
('CS0003', 'Bachelor of Information Technology', 'DP0002', 4, NULL, NULL),
('CS0004', 'Information Technology & Management', 'DP0002', 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_code`, `department_name`, `created_at`, `updated_at`) VALUES
('DP0001', 'Engineering Department', NULL, NULL),
('DP0002', 'IT Department', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `examinor`
-- (See below for the actual view)
--
CREATE TABLE `examinor` (
`reg_no` varchar(15)
,`fname` varchar(30)
,`lname` varchar(30)
,`username` varchar(20)
,`nic` varchar(15)
,`address` varchar(200)
,`mobile` varchar(10)
,`email` varchar(100)
,`active_status` char(1)
,`created_at` timestamp
,`updated_at` timestamp
,`userrole` varchar(5)
);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fixval`
--

CREATE TABLE `fixval` (
  `fixval_id` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fixval_des` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fixval_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` char(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fixval`
--

INSERT INTO `fixval` (`fixval_id`, `fixval_des`, `fixval_type`, `order`, `active`) VALUES
('ADMIN', 'Administrator', 'USERROLE', 4, '1'),
('ED', 'Examination Devisioner', 'USERROLE', 3, '1'),
('LEC', 'Lecturer', 'USERROLE', 2, '1'),
('STU', 'Student', 'USERROLE', 1, '1');

-- --------------------------------------------------------

--
-- Stand-in structure for view `lecturer`
-- (See below for the actual view)
--
CREATE TABLE `lecturer` (
`reg_no` varchar(15)
,`fname` varchar(30)
,`lname` varchar(30)
,`username` varchar(20)
,`nic` varchar(15)
,`address` varchar(200)
,`mobile` varchar(10)
,`email` varchar(100)
,`active_status` char(1)
,`created_at` timestamp
,`updated_at` timestamp
,`userrole` varchar(5)
);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `email`, `password`, `user_role`, `reg_no`, `created_at`, `updated_at`) VALUES
(1, 'hasini@gmail.com', '$2y$10$u7FASUT.szaQfyWg8iXFZOYSry4YqDjzbwD5PMbvcs0ktlde0QSo.', 'STU', 'REGST20000001', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mark`
--

CREATE TABLE `mark` (
  `mark_rowno` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enroll_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_year` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'course year',
  `year` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `continuous_assignment_mark` int(11) DEFAULT NULL,
  `exam_mark` int(11) DEFAULT NULL,
  `ﬁnal_mark` int(11) DEFAULT NULL,
  `grade` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N',
  `status` char(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'N' COMMENT 'after final result  complete - C/repeat - R/absent - AB/assignment not complete - ANC /on going - N',
  `approved_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '1 - approved / 0 - not approved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mark`
--

INSERT INTO `mark` (`mark_rowno`, `reg_no`, `enroll_id`, `current_year`, `year`, `continuous_assignment_mark`, `exam_mark`, `ﬁnal_mark`, `grade`, `status`, `approved_status`, `created_at`, `updated_at`) VALUES
(4, 'REGST20000002', 'SUB20000002', '1', '2020', 40, NULL, NULL, 'N', 'N', '0', '2020-03-07 11:15:22', '2020-03-21 12:10:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_25_032200_create_registrations_table', 1),
(5, '2020_01_25_022548_create_logins_table', 2),
(6, '2020_01_27_000442_create_studen_dtls_table', 3),
(7, '2020_02_01_150941_create_module_dtls_table', 4),
(8, '2020_02_01_152338_create_modules_to_enrolls_table', 5),
(9, '2020_02_01_162231_create_marks_table', 6),
(10, '2020_02_06_135558_create_stu_result_summaries_table', 7),
(11, '2020_02_06_141712_create_apptitute_marks_table', 8),
(12, '2020_02_06_142238_create_repeat_applies_table', 9),
(13, '2020_02_08_031912_create_fixvals_table', 10),
(14, '2020_02_16_172134_create_departments_table', 11),
(15, '2020_02_16_174643_create_courses_table', 12),
(16, '2020_02_27_001815_create_tempory_enrolls_table', 13),
(17, '2020_03_03_000321_create_uploaded_marksheets_table', 14),
(18, '2020_03_11_170848_create_student_g_p_a_s_table', 15),
(19, '2020_03_13_005910_create_tempory_repeat_enrolls_table', 16);

-- --------------------------------------------------------

--
-- Table structure for table `modules_to_enroll`
--

CREATE TABLE `modules_to_enroll` (
  `enroll_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_year` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `module_code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lecture_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `continuous_assginment_ratio` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exam_mark_ratio` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules_to_enroll`
--

INSERT INTO `modules_to_enroll` (`enroll_id`, `department`, `course`, `course_year`, `semester`, `module_code`, `lecture_id`, `continuous_assginment_ratio`, `exam_mark_ratio`, `created_at`, `updated_at`) VALUES
('SUB20000001', 'DP0001', 'CS0001', 1, 2, 'MOD20000001', 'LEC20000004', '15', '85', '2020-02-25 09:47:24', '2020-02-25 09:47:24'),
('SUB20000002', 'DP0001', 'CS0001', 1, 1, 'MOD20000002', 'LEC20000004', '40', '60', NULL, NULL),
('SUB20000003', 'DP0001', 'CS0001', 1, 2, 'MOD20000003', 'LEC20000004', '35', '65', NULL, NULL),
('SUB20000004', 'DP0001', 'CS0001', 1, 1, 'MOD20000004', 'LEC20000004', '40', '60', NULL, NULL),
('SUB20000005', 'DP0001', 'CS0002', 1, 1, 'MOD20000005', 'LEC20000005', '40', '60', NULL, NULL),
('SUB20000006', 'DP0001', 'CS0001', 2, 1, 'MOD20000006', 'LEC20000004', '40', '60', NULL, NULL),
('SUB20000007', 'DP0001', 'CS0001', 2, 1, 'MOD20000007', 'LEC20000005', '40', '60', NULL, NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `module_description`
-- (See below for the actual view)
--
CREATE TABLE `module_description` (
`enroll_id` varchar(15)
,`department` varchar(50)
,`course` varchar(10)
,`course_year` int(11)
,`semester` int(11)
,`module_code` varchar(15)
,`lecture_id` varchar(15)
,`continuous_assginment_ratio` varchar(7)
,`exam_mark_ratio` varchar(7)
,`created_at` timestamp
,`updated_at` timestamp
,`module_name` varchar(100)
,`no_of_credit` int(11)
,`repeat_price` double(7,2)
);

-- --------------------------------------------------------

--
-- Table structure for table `module_dtl`
--

CREATE TABLE `module_dtl` (
  `module_code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_credit` int(11) NOT NULL,
  `repeat_price` double(7,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `module_dtl`
--

INSERT INTO `module_dtl` (`module_code`, `module_name`, `no_of_credit`, `repeat_price`, `created_at`, `updated_at`) VALUES
('MOD20000001', 'Software Engineering', 3, 2500.00, '2020-02-22 13:13:12', '2020-02-22 13:13:12'),
('MOD20000002', 'Statistics for Mathematics', 4, 4000.00, '2020-02-24 11:51:02', '2020-02-24 11:51:02'),
('MOD20000003', 'Human Resource Management\r\n', 2, 2000.00, NULL, NULL),
('MOD20000004', 'Machine Basic\r\n', 2, 2000.00, NULL, NULL),
('MOD20000005', 'Professional Communication & negotiation skills for Quantity Surveying practice', 3, 2500.00, NULL, NULL),
('MOD20000006', 'Management Studies for Construction and the Built Environment', 3, 2500.00, NULL, NULL),
('MOD20000007', 'Construction Project Planning and Design Management', 3, 2500.00, NULL, NULL),
('MOD20000008', 'Building Technology and Environmental Design for Construction', 3, 2500.00, NULL, NULL),
('MOD20000009', 'Civil Engineering technology and structural design', 3, 2500.00, NULL, NULL),
('MOD20000010', 'Fundamentals of Computer Aided Design & Model-based process', 3, 2500.00, NULL, NULL),
('MOD20000011', 'Material science and structural integrity in building & engineering design', 3, 2500.00, NULL, NULL),
('MOD20000012', 'Principles of Building surveying, maintenance, conversion and Adaptation', 4, 3000.00, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nic` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`reg_no`, `fname`, `lname`, `username`, `nic`, `address`, `mobile`, `email`, `active_status`, `created_at`, `updated_at`) VALUES
('ED20000001', 'Sanduni', 'Rameshika', 'sandu', '958756123v', 'Polhene gedara, Wattegama , Dickwella', '0711356909', 'kavirathne.s@gmail.com', '1', '2020-02-19 19:58:26', '2020-02-19 19:58:26'),
('ED20000002', 'test', 'ed1', 'tested1', '878954621v', 'tested1', '045698563', 'tested1@gmail.com', '1', '2020-02-21 08:30:35', '2020-02-21 08:30:35'),
('LEC20000004', 'Damith', 'Rathnayake', 'Anu', '856545233v', 'No.258 , Kothalawala junction , Embilipitiya', '07765789', 'danu@gmail.com', '1', '2020-02-19 18:36:53', '2020-02-19 18:36:53'),
('LEC20000005', 'test', 'lecturer 1', 'testlecturer', '54567894v', 'test lecturer', '0147894561', 'testlec1@gmail.com', '1', '2020-02-21 08:28:35', '2020-02-21 08:28:35'),
('REGST20000001', 'ghjkl', 'gffegfw', '', '54654s', 'dgdfuif', '684416', 'hfigfiwgix@gmail.omc', '1', NULL, NULL),
('REGST20000002', 'Hasini', 'Ahayawardhana', 'Madhu', '9571309335v', '\'Shanthi\' , Puwakwatta , Kamburugamuwa', '0710147030', 'madhumani7317@gmail.com', '1', NULL, NULL),
('REGST20000003', 'Rasini', 'Abhayawardhana', 'madhu', '8945657v', 'No 399 , Kotalawala Junction , Embilipitiya', '0711743367', 'madhu89@gmail.com', '1', '2020-02-16 13:12:55', '2020-02-16 13:12:55'),
('REGST20000004', 'test', 'student1', 'teststu1', '954565412v', 'test stu', '0711236545', 'teststu1@gmail.com', '1', '2020-02-21 08:26:35', '2020-02-21 08:26:35'),
('REGST20000005', 'Sasindu', 'Wijesinghe', 'sasi', '934565789v', '789 , Negambo , Colombo', '0147896545', 'sasiw@gmail.com', '1', '2020-02-24 12:10:59', '2020-02-24 12:10:59');

-- --------------------------------------------------------

--
-- Table structure for table `repeat_apply`
--

CREATE TABLE `repeat_apply` (
  `application_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `repeat_fee` decimal(7,2) NOT NULL,
  `receipt_img` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_date` date NOT NULL,
  `approved_date` date NOT NULL,
  `approved_by` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `approved_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '1 - approved / 0 - not approved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `repeat_apply`
--

INSERT INTO `repeat_apply` (`application_id`, `reg_no`, `repeat_fee`, `receipt_img`, `apply_date`, `approved_date`, `approved_by`, `approved_status`, `created_at`, `updated_at`) VALUES
('APP20000001', 'REGST20000002', '4000.00', 'repeatPayments/REGST20000002_20200321174028.jpeg', '2020-03-21', '2020-03-21', 'ED20000002', '1', '2020-03-21 12:10:28', '2020-03-21 13:25:07');

-- --------------------------------------------------------

--
-- Table structure for table `repeat_module`
--

CREATE TABLE `repeat_module` (
  `row_no` bigint(20) UNSIGNED NOT NULL,
  `application_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enroll_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `repeat_module`
--

INSERT INTO `repeat_module` (`row_no`, `application_id`, `enroll_id`, `created_at`, `updated_at`) VALUES
(3, 'APP20000001', 'SUB20000002', '2020-03-21 12:10:28', '2020-03-21 12:10:28');

-- --------------------------------------------------------

--
-- Stand-in structure for view `student`
-- (See below for the actual view)
--
CREATE TABLE `student` (
`reg_no` varchar(15)
,`fname` varchar(30)
,`lname` varchar(30)
,`username` varchar(20)
,`nic` varchar(15)
,`address` varchar(200)
,`mobile` varchar(10)
,`email` varchar(100)
,`active_status` char(1)
,`created_at` timestamp
,`updated_at` timestamp
,`userrole` varchar(5)
,`department` varchar(100)
,`department_name` varchar(100)
,`course` varchar(100)
,`course_name` varchar(100)
,`current_year` int(11)
,`current_semester` int(11)
,`started_date` date
,`end_year` int(11)
,`is_apptitude_student` char(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `student_g_p_a_s`
--

CREATE TABLE `student_g_p_a_s` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester` int(11) NOT NULL,
  `gpa` decimal(5,4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `student_g_p_a_s`
--

INSERT INTO `student_g_p_a_s` (`id`, `reg_no`, `semester`, `gpa`, `created_at`, `updated_at`) VALUES
(5, 'REGST20000002', 1, '4.0000', '2020-03-11 12:15:06', '2020-03-11 12:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `studen_dtl`
--

CREATE TABLE `studen_dtl` (
  `stu_dtl_id` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_year` int(11) NOT NULL,
  `current_semester` int(11) NOT NULL,
  `started_date` date NOT NULL,
  `end_year` int(11) DEFAULT NULL,
  `is_apptitude_student` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `studen_dtl`
--

INSERT INTO `studen_dtl` (`stu_dtl_id`, `reg_no`, `department`, `course`, `current_year`, `current_semester`, `started_date`, `end_year`, `is_apptitude_student`, `created_at`, `updated_at`) VALUES
(2, 'REGST20000002', 'DP0001', 'CS0001', 1, 1, '2020-02-06', NULL, '0', NULL, NULL),
(3, 'REGST20000003', 'DP0001', 'CS0001', 1, 1, '2020-01-28', NULL, '0', '2020-02-16 13:12:55', '2020-02-16 13:12:55'),
(4, 'REGST20000004', 'DP0001', 'CS0001', 1, 1, '2020-02-06', NULL, '1', '2020-02-21 08:26:35', '2020-02-21 08:26:35'),
(5, 'REGST20000005', 'DP0002', 'CS0004', 1, 1, '2020-01-28', NULL, '0', '2020-02-24 12:10:59', '2020-02-24 12:10:59');

-- --------------------------------------------------------

--
-- Table structure for table `stu_gpa_summary`
--

CREATE TABLE `stu_gpa_summary` (
  `rowno` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `overall_gpa` decimal(5,4) DEFAULT NULL,
  `class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complete_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0-notcomplete / 1-complete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stu_gpa_summary`
--

INSERT INTO `stu_gpa_summary` (`rowno`, `reg_no`, `overall_gpa`, `class`, `complete_status`, `created_at`, `updated_at`) VALUES
(1, 'REGST20000002', '4.0000', NULL, '1', '2020-03-09 06:35:10', '2020-03-11 13:01:25');

-- --------------------------------------------------------

--
-- Table structure for table `stu_result_summary_20200311`
--

CREATE TABLE `stu_result_summary_20200311` (
  `rowno` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sem_1_gpa` decimal(5,4) DEFAULT NULL,
  `sem_2_gpa` decimal(5,4) DEFAULT NULL,
  `sem_3_gpa` decimal(5,4) DEFAULT NULL,
  `sem_4_gpa` decimal(5,4) DEFAULT NULL,
  `sem_5_gpa` decimal(5,4) DEFAULT NULL,
  `sem_6_gpa` decimal(5,4) DEFAULT NULL,
  `overall_gpa` decimal(5,4) DEFAULT NULL,
  `class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `complete_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0-notcomplete / 1-complete',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stu_result_summary_20200311`
--

INSERT INTO `stu_result_summary_20200311` (`rowno`, `reg_no`, `sem_1_gpa`, `sem_2_gpa`, `sem_3_gpa`, `sem_4_gpa`, `sem_5_gpa`, `sem_6_gpa`, `overall_gpa`, `class`, `complete_status`, `created_at`, `updated_at`) VALUES
(1, 'REGST20000002', '4.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', '2020-03-09 06:35:10', '2020-03-10 18:00:55');

-- --------------------------------------------------------

--
-- Table structure for table `tempory_enrolls`
--

CREATE TABLE `tempory_enrolls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_year` int(11) NOT NULL,
  `course_semester` int(11) NOT NULL,
  `enroll_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tempory_repeat_enrolls`
--

CREATE TABLE `tempory_repeat_enrolls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enroll_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `uploaded_marksheets`
--

CREATE TABLE `uploaded_marksheets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `enroll_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `document_path` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `semester_yr` int(11) NOT NULL,
  `created_by` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved_by` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `approved_at` date NOT NULL DEFAULT '0000-00-00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `uploaded_marksheets`
--

INSERT INTO `uploaded_marksheets` (`id`, `enroll_id`, `document_path`, `semester_yr`, `created_by`, `approved_by`, `approved_at`, `created_at`, `updated_at`) VALUES
(6, 'SUB20000002', 'markSheets/DP0001_CS0001_MOD20000002_20200307164708.xlsx', 2020, 'LEC20000005', 'ED20000002', '2020-03-07', '2020-03-07 11:17:09', '2020-03-07 11:18:59');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userrole` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `userrole`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hasini', 'admin@gmail.com', 'ADMIN', NULL, '$2y$10$u7FASUT.szaQfyWg8iXFZOYSry4YqDjzbwD5PMbvcs0ktlde0QSo.', NULL, '2020-01-31 11:11:42', '2020-01-31 11:11:42'),
(5, 'Madhu', 'madhumani7317@gmail.com', 'STU', NULL, '$2y$10$AA94MkNRg3KBOcgSKY4EmOUK8U4MSiAFey5F45nbNzxS4s.dgnEcC', NULL, NULL, NULL),
(9, 'madhu', 'madhu89@gmail.com', 'STU', NULL, '$2y$10$D6nwBHH03YD.3npIn8zM5.8/CtsyLkNMVTQ7SYysNwUnoksfBj43G', NULL, '2020-02-16 13:12:55', '2020-02-16 13:12:55'),
(10, 'Anu', 'danu@gmail.com', 'LEC', NULL, '$2y$10$.ZPG2doeXQ5RXE341QFW/O90Rq7U/PcizrFFSN/S8xjqoLj6h/gla', NULL, '2020-02-19 18:36:54', '2020-02-19 18:36:54'),
(11, 'sandu', 'kavirathne.s@gmail.com', 'ED', NULL, '$2y$10$fpUlCA0as8jN5uvmd4BG3eLvdmCtTs1cWD2X5y4/cMzCysAIUlIi6', NULL, '2020-02-19 19:58:26', '2020-02-19 19:58:26'),
(12, 'teststu1', 'teststu1@gmail.com', 'STU', NULL, '$2y$10$xoUf7Wix2AH3YuSrg7xKh.8T4azty.rHDUUJ38d50xx.J6lnXhRp6', NULL, '2020-02-21 08:26:35', '2020-02-21 08:26:35'),
(13, 'testlecturer', 'testlec1@gmail.com', 'LEC', NULL, '$2y$10$TSx53BkoFrNYvdoq2pvkOuRhKlV4XVvILv88oWmIC806WhDH.4Ux.', NULL, '2020-02-21 08:28:35', '2020-02-21 08:28:35'),
(14, 'tested1', 'tested1@gmail.com', 'ED', NULL, '$2y$10$04RUNV5uSNNERX1Rh1Po8updn9K3sDcSB0ci8nmgWCF.XN7nIb5PK', NULL, '2020-02-21 08:30:35', '2020-02-21 08:30:35'),
(15, 'sasi', 'sasiw@gmail.com', 'STU', NULL, '$2y$10$5Yq8i9QNfDRLRzFMzE990OxzUMfvGP0sCoS9lBw8D9wB1D9s.wprK', NULL, '2020-02-24 12:10:59', '2020-02-24 12:10:59');

-- --------------------------------------------------------

--
-- Table structure for table `users_20200208`
--

CREATE TABLE `users_20200208` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_20200208`
--

INSERT INTO `users_20200208` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hasini', 'admin@gmail.com', NULL, '$2y$10$u7FASUT.szaQfyWg8iXFZOYSry4YqDjzbwD5PMbvcs0ktlde0QSo.', NULL, '2020-01-31 11:11:42', '2020-01-31 11:11:42');

-- --------------------------------------------------------

--
-- Structure for view `examinor`
--
DROP TABLE IF EXISTS `examinor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `examinor`  AS  select `registration`.`reg_no` AS `reg_no`,`registration`.`fname` AS `fname`,`registration`.`lname` AS `lname`,`registration`.`username` AS `username`,`registration`.`nic` AS `nic`,`registration`.`address` AS `address`,`registration`.`mobile` AS `mobile`,`registration`.`email` AS `email`,`registration`.`active_status` AS `active_status`,`registration`.`created_at` AS `created_at`,`registration`.`updated_at` AS `updated_at`,`users`.`userrole` AS `userrole` from (`registration` join `users` on((`users`.`email` = `registration`.`email`))) where (`users`.`userrole` = 'ED') ;

-- --------------------------------------------------------

--
-- Structure for view `lecturer`
--
DROP TABLE IF EXISTS `lecturer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `lecturer`  AS  select `registration`.`reg_no` AS `reg_no`,`registration`.`fname` AS `fname`,`registration`.`lname` AS `lname`,`registration`.`username` AS `username`,`registration`.`nic` AS `nic`,`registration`.`address` AS `address`,`registration`.`mobile` AS `mobile`,`registration`.`email` AS `email`,`registration`.`active_status` AS `active_status`,`registration`.`created_at` AS `created_at`,`registration`.`updated_at` AS `updated_at`,`users`.`userrole` AS `userrole` from (`registration` join `users` on((`users`.`email` = `registration`.`email`))) where (`users`.`userrole` = 'LEC') ;

-- --------------------------------------------------------

--
-- Structure for view `module_description`
--
DROP TABLE IF EXISTS `module_description`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `module_description`  AS  select `modules_to_enroll`.`enroll_id` AS `enroll_id`,`modules_to_enroll`.`department` AS `department`,`modules_to_enroll`.`course` AS `course`,`modules_to_enroll`.`course_year` AS `course_year`,`modules_to_enroll`.`semester` AS `semester`,`modules_to_enroll`.`module_code` AS `module_code`,`modules_to_enroll`.`lecture_id` AS `lecture_id`,`modules_to_enroll`.`continuous_assginment_ratio` AS `continuous_assginment_ratio`,`modules_to_enroll`.`exam_mark_ratio` AS `exam_mark_ratio`,`modules_to_enroll`.`created_at` AS `created_at`,`modules_to_enroll`.`updated_at` AS `updated_at`,`module_dtl`.`module_name` AS `module_name`,`module_dtl`.`no_of_credit` AS `no_of_credit`,`module_dtl`.`repeat_price` AS `repeat_price` from (`modules_to_enroll` join `module_dtl` on((`module_dtl`.`module_code` = `modules_to_enroll`.`module_code`))) ;

-- --------------------------------------------------------

--
-- Structure for view `student`
--
DROP TABLE IF EXISTS `student`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student`  AS  select `registration`.`reg_no` AS `reg_no`,`registration`.`fname` AS `fname`,`registration`.`lname` AS `lname`,`registration`.`username` AS `username`,`registration`.`nic` AS `nic`,`registration`.`address` AS `address`,`registration`.`mobile` AS `mobile`,`registration`.`email` AS `email`,`registration`.`active_status` AS `active_status`,`registration`.`created_at` AS `created_at`,`registration`.`updated_at` AS `updated_at`,`users`.`userrole` AS `userrole`,`studen_dtl`.`department` AS `department`,`departments`.`department_name` AS `department_name`,`studen_dtl`.`course` AS `course`,`courses`.`course_name` AS `course_name`,`studen_dtl`.`current_year` AS `current_year`,`studen_dtl`.`current_semester` AS `current_semester`,`studen_dtl`.`started_date` AS `started_date`,`studen_dtl`.`end_year` AS `end_year`,`studen_dtl`.`is_apptitude_student` AS `is_apptitude_student` from ((((`registration` join `users` on((`users`.`email` = `registration`.`email`))) join `studen_dtl` on((`studen_dtl`.`reg_no` = `registration`.`reg_no`))) join `departments` on((`departments`.`department_code` = `studen_dtl`.`department`))) join `courses` on((`courses`.`course_code` = `studen_dtl`.`course`))) where (`users`.`userrole` = 'STU') ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apptitute_mark`
--
ALTER TABLE `apptitute_mark`
  ADD PRIMARY KEY (`apptitute_row_no`),
  ADD UNIQUE KEY `apptitute_mark_reg_no_unique` (`reg_no`),
  ADD KEY `apptitute_mark_module_code_foreign` (`module_code`);

--
-- Indexes for table `continuous_assignment_mark`
--
ALTER TABLE `continuous_assignment_mark`
  ADD PRIMARY KEY (`rowno`),
  ADD KEY `continuous_assignment_mark_reg_no_foreign` (`reg_no`),
  ADD KEY `continuous_assignment_mark_mark_rowno_foreign` (`mark_rowno`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`course_code`),
  ADD KEY `courses_department_code_foreign` (`department_code`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_code`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixval`
--
ALTER TABLE `fixval`
  ADD PRIMARY KEY (`fixval_id`,`fixval_type`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_reg_no_unique` (`reg_no`);

--
-- Indexes for table `mark`
--
ALTER TABLE `mark`
  ADD PRIMARY KEY (`mark_rowno`),
  ADD UNIQUE KEY `mark_reg_no_unique` (`reg_no`),
  ADD UNIQUE KEY `mark_enroll_id_unique` (`enroll_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules_to_enroll`
--
ALTER TABLE `modules_to_enroll`
  ADD PRIMARY KEY (`enroll_id`),
  ADD KEY `fk_modenrol_lecture_id` (`lecture_id`),
  ADD KEY `fk_modenrol_modcd` (`module_code`),
  ADD KEY `fk_modenrol_department` (`department`),
  ADD KEY `fk_modenrol_course` (`course`);

--
-- Indexes for table `module_dtl`
--
ALTER TABLE `module_dtl`
  ADD PRIMARY KEY (`module_code`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`reg_no`),
  ADD UNIQUE KEY `registration_nic_unique` (`nic`),
  ADD UNIQUE KEY `registration_email_unique` (`email`);

--
-- Indexes for table `repeat_apply`
--
ALTER TABLE `repeat_apply`
  ADD PRIMARY KEY (`application_id`),
  ADD KEY `repeat_apply_reg_no_foreign` (`reg_no`);

--
-- Indexes for table `repeat_module`
--
ALTER TABLE `repeat_module`
  ADD PRIMARY KEY (`row_no`),
  ADD KEY `repeat_module_application_id_foreign` (`application_id`),
  ADD KEY `repeat_module_enroll_id_foreign` (`enroll_id`);

--
-- Indexes for table `student_g_p_a_s`
--
ALTER TABLE `student_g_p_a_s`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_g_p_a_s_reg_no_foreign` (`reg_no`);

--
-- Indexes for table `studen_dtl`
--
ALTER TABLE `studen_dtl`
  ADD PRIMARY KEY (`stu_dtl_id`),
  ADD UNIQUE KEY `studen_dtl_reg_no_unique` (`reg_no`),
  ADD KEY `studen_dtl_department_foreign` (`department`),
  ADD KEY `studen_dtl_course_foreign` (`course`);

--
-- Indexes for table `stu_gpa_summary`
--
ALTER TABLE `stu_gpa_summary`
  ADD PRIMARY KEY (`rowno`),
  ADD UNIQUE KEY `stu_result_summary_reg_no_unique` (`reg_no`);

--
-- Indexes for table `stu_result_summary_20200311`
--
ALTER TABLE `stu_result_summary_20200311`
  ADD PRIMARY KEY (`rowno`),
  ADD UNIQUE KEY `stu_result_summary_reg_no_unique` (`reg_no`);

--
-- Indexes for table `tempory_enrolls`
--
ALTER TABLE `tempory_enrolls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tempory_enrolls_regno_foreign` (`reg_no`),
  ADD KEY `tempory_enrolls_enroll_id_foreign` (`enroll_id`);

--
-- Indexes for table `tempory_repeat_enrolls`
--
ALTER TABLE `tempory_repeat_enrolls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tempory_repeat_enrolls_reg_no_foreign` (`reg_no`),
  ADD KEY `tempory_repeat_enrolls_enroll_id_foreign` (`enroll_id`);

--
-- Indexes for table `uploaded_marksheets`
--
ALTER TABLE `uploaded_marksheets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uploaded_marksheets_enroll_id_foreign` (`enroll_id`),
  ADD KEY `uploaded_marksheets_created_by_foreign` (`created_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_20200208`
--
ALTER TABLE `users_20200208`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apptitute_mark`
--
ALTER TABLE `apptitute_mark`
  MODIFY `apptitute_row_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `continuous_assignment_mark`
--
ALTER TABLE `continuous_assignment_mark`
  MODIFY `rowno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mark`
--
ALTER TABLE `mark`
  MODIFY `mark_rowno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `repeat_module`
--
ALTER TABLE `repeat_module`
  MODIFY `row_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_g_p_a_s`
--
ALTER TABLE `student_g_p_a_s`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `studen_dtl`
--
ALTER TABLE `studen_dtl`
  MODIFY `stu_dtl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stu_gpa_summary`
--
ALTER TABLE `stu_gpa_summary`
  MODIFY `rowno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `stu_result_summary_20200311`
--
ALTER TABLE `stu_result_summary_20200311`
  MODIFY `rowno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tempory_enrolls`
--
ALTER TABLE `tempory_enrolls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tempory_repeat_enrolls`
--
ALTER TABLE `tempory_repeat_enrolls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `uploaded_marksheets`
--
ALTER TABLE `uploaded_marksheets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users_20200208`
--
ALTER TABLE `users_20200208`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `apptitute_mark`
--
ALTER TABLE `apptitute_mark`
  ADD CONSTRAINT `apptitute_mark_module_code_foreign` FOREIGN KEY (`module_code`) REFERENCES `module_dtl` (`module_code`),
  ADD CONSTRAINT `apptitute_mark_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `continuous_assignment_mark`
--
ALTER TABLE `continuous_assignment_mark`
  ADD CONSTRAINT `continuous_assignment_mark_mark_rowno_foreign` FOREIGN KEY (`mark_rowno`) REFERENCES `mark` (`mark_rowno`),
  ADD CONSTRAINT `continuous_assignment_mark_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `courses_department_code_foreign` FOREIGN KEY (`department_code`) REFERENCES `departments` (`department_code`);

--
-- Constraints for table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `mark`
--
ALTER TABLE `mark`
  ADD CONSTRAINT `mark_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `modules_to_enroll` (`enroll_id`),
  ADD CONSTRAINT `mark_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `modules_to_enroll`
--
ALTER TABLE `modules_to_enroll`
  ADD CONSTRAINT `fk_modenrol_course` FOREIGN KEY (`course`) REFERENCES `courses` (`course_code`),
  ADD CONSTRAINT `fk_modenrol_department` FOREIGN KEY (`department`) REFERENCES `departments` (`department_code`),
  ADD CONSTRAINT `fk_modenrol_lecture_id` FOREIGN KEY (`lecture_id`) REFERENCES `registration` (`reg_no`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_modenrol_modcd` FOREIGN KEY (`module_code`) REFERENCES `module_dtl` (`module_code`);

--
-- Constraints for table `repeat_apply`
--
ALTER TABLE `repeat_apply`
  ADD CONSTRAINT `repeat_apply_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `repeat_module`
--
ALTER TABLE `repeat_module`
  ADD CONSTRAINT `repeat_module_application_id_foreign` FOREIGN KEY (`application_id`) REFERENCES `repeat_apply` (`application_id`),
  ADD CONSTRAINT `repeat_module_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `modules_to_enroll` (`enroll_id`);

--
-- Constraints for table `student_g_p_a_s`
--
ALTER TABLE `student_g_p_a_s`
  ADD CONSTRAINT `student_g_p_a_s_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `studen_dtl`
--
ALTER TABLE `studen_dtl`
  ADD CONSTRAINT `studen_dtl_course_foreign` FOREIGN KEY (`course`) REFERENCES `courses` (`course_code`),
  ADD CONSTRAINT `studen_dtl_department_foreign` FOREIGN KEY (`department`) REFERENCES `departments` (`department_code`),
  ADD CONSTRAINT `studen_dtl_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `stu_result_summary_20200311`
--
ALTER TABLE `stu_result_summary_20200311`
  ADD CONSTRAINT `stu_result_summary_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `tempory_enrolls`
--
ALTER TABLE `tempory_enrolls`
  ADD CONSTRAINT `tempory_enrolls_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `modules_to_enroll` (`enroll_id`),
  ADD CONSTRAINT `tempory_enrolls_regno_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `tempory_repeat_enrolls`
--
ALTER TABLE `tempory_repeat_enrolls`
  ADD CONSTRAINT `tempory_repeat_enrolls_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `modules_to_enroll` (`enroll_id`),
  ADD CONSTRAINT `tempory_repeat_enrolls_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `uploaded_marksheets`
--
ALTER TABLE `uploaded_marksheets`
  ADD CONSTRAINT `uploaded_marksheets_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `registration` (`reg_no`),
  ADD CONSTRAINT `uploaded_marksheets_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `modules_to_enroll` (`enroll_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
