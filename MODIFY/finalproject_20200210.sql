-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2020 at 01:06 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `apptitute_mark`
--

CREATE TABLE `apptitute_mark` (
  `apptitute_row_no` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_year` int(11) NOT NULL,
  `module_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `continuous_assignment_mark`
--

CREATE TABLE `continuous_assignment_mark` (
  `rowno` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mark_rowno` bigint(10) UNSIGNED NOT NULL,
  `current_date` int(11) NOT NULL,
  `assignment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `assignment_mark` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fixval`
--

CREATE TABLE `fixval` (
  `fixval_id` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fixval_des` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fixval_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `active` char(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fixval`
--

INSERT INTO `fixval` (`fixval_id`, `fixval_des`, `fixval_type`, `order`, `active`) VALUES
('STU', 'Student', 'USERROLE', 1, '1');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_role` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `email`, `password`, `user_role`, `reg_no`, `created_at`, `updated_at`) VALUES
(1, 'hasini@gmail.com', '$2y$10$u7FASUT.szaQfyWg8iXFZOYSry4YqDjzbwD5PMbvcs0ktlde0QSo.', 'STU', 'REGST20000001', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mark`
--

CREATE TABLE `mark` (
  `mark_rowno` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enroll_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_year` int(11) NOT NULL,
  `continuous_assignment_mark` int(11) NOT NULL,
  `exam_mark` int(11) NOT NULL,
  `ﬁnal_mark` int(11) NOT NULL,
  `grade` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_25_032200_create_registrations_table', 1),
(5, '2020_01_25_022548_create_logins_table', 2),
(6, '2020_01_27_000442_create_studen_dtls_table', 3),
(7, '2020_02_01_150941_create_module_dtls_table', 4),
(8, '2020_02_01_152338_create_modules_to_enrolls_table', 5),
(9, '2020_02_01_162231_create_marks_table', 6),
(10, '2020_02_06_135558_create_stu_result_summaries_table', 7),
(11, '2020_02_06_141712_create_apptitute_marks_table', 8),
(12, '2020_02_06_142238_create_repeat_applies_table', 9),
(13, '2020_02_08_031912_create_fixvals_table', 10);

-- --------------------------------------------------------

--
-- Table structure for table `modules_to_enroll`
--

CREATE TABLE `modules_to_enroll` (
  `enroll_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course_year` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `module_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lecture_id` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `continuous_assginment_ratio` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exam_mark_ratio` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `module_dtl`
--

CREATE TABLE `module_dtl` (
  `module_code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_credit` int(11) NOT NULL,
  `repeat_price` double(7,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nic` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`reg_no`, `fname`, `lname`, `username`, `nic`, `address`, `mobile`, `email`, `active_status`, `created_at`, `updated_at`) VALUES
('REGST20000001', 'ghjkl', 'gffegfw', '', '54654s', 'dgdfuif', '684416', 'hfigfiwgix@gmail.omc', '1', NULL, NULL),
('REGST20000002', 'Hasini', 'Ahayawardhana', 'Madhu', '9571309335v', '\'Shanthi\' , Puwakwatta , Kamburugamuwa', '0710147030', 'madhumani7317@gmail.com', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `repeat_apply`
--

CREATE TABLE `repeat_apply` (
  `application_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `repeat_fee` decimal(7,2) NOT NULL,
  `payment_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_date` date NOT NULL,
  `registar_approved` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved_date` date NOT NULL,
  `approved_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `repeat_module`
--

CREATE TABLE `repeat_module` (
  `row_no` bigint(20) UNSIGNED NOT NULL,
  `application_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enroll_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `student`
-- (See below for the actual view)
--
CREATE TABLE `student` (
`reg_no` varchar(15)
,`fname` varchar(30)
,`lname` varchar(30)
,`username` varchar(20)
,`nic` varchar(15)
,`address` varchar(200)
,`mobile` varchar(10)
,`email` varchar(100)
,`active_status` char(1)
,`created_at` timestamp
,`updated_at` timestamp
,`userrole` varchar(5)
,`department` varchar(100)
,`course` varchar(100)
,`current_year` int(11)
,`current_semester` int(11)
,`started_date` date
,`end_year` int(11)
,`is_apptitude_student` char(1)
);

-- --------------------------------------------------------

--
-- Table structure for table `studen_dtl`
--

CREATE TABLE `studen_dtl` (
  `stu_dtl_id` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `course` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_year` int(11) NOT NULL,
  `current_semester` int(11) NOT NULL,
  `started_date` date NOT NULL,
  `end_year` int(11) DEFAULT NULL,
  `is_apptitude_student` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `studen_dtl`
--

INSERT INTO `studen_dtl` (`stu_dtl_id`, `reg_no`, `department`, `course`, `current_year`, `current_semester`, `started_date`, `end_year`, `is_apptitude_student`, `created_at`, `updated_at`) VALUES
(2, 'REGST20000002', 'Delaware', 'California', 1, 1, '2020-02-06', NULL, '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stu_result_summary`
--

CREATE TABLE `stu_result_summary` (
  `rowno` bigint(20) UNSIGNED NOT NULL,
  `reg_no` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sem_1_gpa` decimal(5,4) NOT NULL,
  `sem_2_gpa` decimal(5,4) NOT NULL,
  `sem_3_gpa` decimal(5,4) NOT NULL,
  `sem_4_gpa` decimal(5,4) NOT NULL,
  `sem_5_gpa` decimal(5,4) NOT NULL,
  `sem_6_gpa` decimal(5,4) NOT NULL,
  `overall_gpa` decimal(5,4) NOT NULL,
  `class` char(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `complete_status` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userrole` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `userrole`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hasini', 'admin@gmail.com', 'ADMIN', NULL, '$2y$10$u7FASUT.szaQfyWg8iXFZOYSry4YqDjzbwD5PMbvcs0ktlde0QSo.', NULL, '2020-01-31 11:11:42', '2020-01-31 11:11:42'),
(5, 'Madhu', 'madhumani7317@gmail.com', 'STU', NULL, '$2y$10$AA94MkNRg3KBOcgSKY4EmOUK8U4MSiAFey5F45nbNzxS4s.dgnEcC', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_20200208`
--

CREATE TABLE `users_20200208` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users_20200208`
--

INSERT INTO `users_20200208` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hasini', 'admin@gmail.com', NULL, '$2y$10$u7FASUT.szaQfyWg8iXFZOYSry4YqDjzbwD5PMbvcs0ktlde0QSo.', NULL, '2020-01-31 11:11:42', '2020-01-31 11:11:42');

-- --------------------------------------------------------

--
-- Structure for view `student`
--
DROP TABLE IF EXISTS `student`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `student`  AS  select `registration`.`reg_no` AS `reg_no`,`registration`.`fname` AS `fname`,`registration`.`lname` AS `lname`,`registration`.`username` AS `username`,`registration`.`nic` AS `nic`,`registration`.`address` AS `address`,`registration`.`mobile` AS `mobile`,`registration`.`email` AS `email`,`registration`.`active_status` AS `active_status`,`registration`.`created_at` AS `created_at`,`registration`.`updated_at` AS `updated_at`,`users`.`userrole` AS `userrole`,`studen_dtl`.`department` AS `department`,`studen_dtl`.`course` AS `course`,`studen_dtl`.`current_year` AS `current_year`,`studen_dtl`.`current_semester` AS `current_semester`,`studen_dtl`.`started_date` AS `started_date`,`studen_dtl`.`end_year` AS `end_year`,`studen_dtl`.`is_apptitude_student` AS `is_apptitude_student` from ((`registration` join `users` on((`users`.`email` = `registration`.`email`))) join `studen_dtl` on((`studen_dtl`.`reg_no` = `registration`.`reg_no`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apptitute_mark`
--
ALTER TABLE `apptitute_mark`
  ADD PRIMARY KEY (`apptitute_row_no`),
  ADD UNIQUE KEY `apptitute_mark_reg_no_unique` (`reg_no`),
  ADD KEY `apptitute_mark_module_code_foreign` (`module_code`);

--
-- Indexes for table `continuous_assignment_mark`
--
ALTER TABLE `continuous_assignment_mark`
  ADD PRIMARY KEY (`rowno`),
  ADD KEY `continuous_assignment_mark_reg_no_foreign` (`reg_no`),
  ADD KEY `continuous_assignment_mark_mark_rowno_foreign` (`mark_rowno`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixval`
--
ALTER TABLE `fixval`
  ADD PRIMARY KEY (`fixval_id`,`fixval_type`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login_reg_no_unique` (`reg_no`);

--
-- Indexes for table `mark`
--
ALTER TABLE `mark`
  ADD PRIMARY KEY (`mark_rowno`),
  ADD UNIQUE KEY `mark_reg_no_unique` (`reg_no`),
  ADD UNIQUE KEY `mark_enroll_id_unique` (`enroll_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules_to_enroll`
--
ALTER TABLE `modules_to_enroll`
  ADD PRIMARY KEY (`enroll_id`),
  ADD KEY `fk_modenrol_lecture_id` (`lecture_id`),
  ADD KEY `fk_modenrol_modcd` (`module_code`);

--
-- Indexes for table `module_dtl`
--
ALTER TABLE `module_dtl`
  ADD PRIMARY KEY (`module_code`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`reg_no`),
  ADD UNIQUE KEY `registration_nic_unique` (`nic`),
  ADD UNIQUE KEY `registration_email_unique` (`email`);

--
-- Indexes for table `repeat_apply`
--
ALTER TABLE `repeat_apply`
  ADD PRIMARY KEY (`application_id`),
  ADD UNIQUE KEY `repeat_apply_reg_no_unique` (`reg_no`);

--
-- Indexes for table `repeat_module`
--
ALTER TABLE `repeat_module`
  ADD PRIMARY KEY (`row_no`),
  ADD KEY `repeat_module_application_id_foreign` (`application_id`),
  ADD KEY `repeat_module_enroll_id_foreign` (`enroll_id`);

--
-- Indexes for table `studen_dtl`
--
ALTER TABLE `studen_dtl`
  ADD PRIMARY KEY (`stu_dtl_id`),
  ADD UNIQUE KEY `studen_dtl_reg_no_unique` (`reg_no`);

--
-- Indexes for table `stu_result_summary`
--
ALTER TABLE `stu_result_summary`
  ADD PRIMARY KEY (`rowno`),
  ADD UNIQUE KEY `stu_result_summary_reg_no_unique` (`reg_no`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_20200208`
--
ALTER TABLE `users_20200208`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apptitute_mark`
--
ALTER TABLE `apptitute_mark`
  MODIFY `apptitute_row_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `continuous_assignment_mark`
--
ALTER TABLE `continuous_assignment_mark`
  MODIFY `rowno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mark`
--
ALTER TABLE `mark`
  MODIFY `mark_rowno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `repeat_module`
--
ALTER TABLE `repeat_module`
  MODIFY `row_no` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `studen_dtl`
--
ALTER TABLE `studen_dtl`
  MODIFY `stu_dtl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stu_result_summary`
--
ALTER TABLE `stu_result_summary`
  MODIFY `rowno` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_20200208`
--
ALTER TABLE `users_20200208`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `apptitute_mark`
--
ALTER TABLE `apptitute_mark`
  ADD CONSTRAINT `apptitute_mark_module_code_foreign` FOREIGN KEY (`module_code`) REFERENCES `module_dtl` (`module_code`),
  ADD CONSTRAINT `apptitute_mark_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `continuous_assignment_mark`
--
ALTER TABLE `continuous_assignment_mark`
  ADD CONSTRAINT `continuous_assignment_mark_mark_rowno_foreign` FOREIGN KEY (`mark_rowno`) REFERENCES `mark` (`mark_rowno`),
  ADD CONSTRAINT `continuous_assignment_mark_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `login`
--
ALTER TABLE `login`
  ADD CONSTRAINT `login_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `mark`
--
ALTER TABLE `mark`
  ADD CONSTRAINT `mark_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `modules_to_enroll` (`enroll_id`),
  ADD CONSTRAINT `mark_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `modules_to_enroll`
--
ALTER TABLE `modules_to_enroll`
  ADD CONSTRAINT `fk_modenrol_lecture_id` FOREIGN KEY (`lecture_id`) REFERENCES `registration` (`reg_no`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_modenrol_modcd` FOREIGN KEY (`module_code`) REFERENCES `modules_to_enroll` (`enroll_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `repeat_apply`
--
ALTER TABLE `repeat_apply`
  ADD CONSTRAINT `repeat_apply_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `repeat_module`
--
ALTER TABLE `repeat_module`
  ADD CONSTRAINT `repeat_module_application_id_foreign` FOREIGN KEY (`application_id`) REFERENCES `repeat_apply` (`application_id`),
  ADD CONSTRAINT `repeat_module_enroll_id_foreign` FOREIGN KEY (`enroll_id`) REFERENCES `modules_to_enroll` (`enroll_id`);

--
-- Constraints for table `studen_dtl`
--
ALTER TABLE `studen_dtl`
  ADD CONSTRAINT `studen_dtl_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);

--
-- Constraints for table `stu_result_summary`
--
ALTER TABLE `stu_result_summary`
  ADD CONSTRAINT `stu_result_summary_reg_no_foreign` FOREIGN KEY (`reg_no`) REFERENCES `registration` (`reg_no`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
