<?php

namespace App\Imports;

use App\Models\ApptituteMark;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class AttitudeTestMarks implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
    	
        return new ApptituteMark([
            // 'rowno',  
            'nic' => $row['nic'],
            'name' => $row['name'],
            'mark'  => $row['mark'],
            'date' => $row['date'],
            'email' => $row['email'],
            'department' => $row['department'],
            'course_code' => $row['course_code'],
            'reg_status'     => $row['reg_status'],
            'email_send_on'  => $row['email_send_on'],
            
        ]);
    }
}
