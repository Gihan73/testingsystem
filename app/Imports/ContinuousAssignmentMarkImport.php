<?php

namespace App\Imports;

use App\Models\ContinuousAssignmentMark;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ContinuousAssignmentMarkImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new ContinuousAssignmentMark([
            // 'rowno',                
            'reg_no'           => $row['reg_no'],
            'mark_rowno'       => $row['mark_rowno'],
            'current_date'     => $row['current_date'],
            'assignment_type'  => $row['assignment_type'],
            'assignment_mark'  => $row['assignment_mark'],
        ]);
    }
}
