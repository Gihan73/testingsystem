<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ModuleDtl;
use App\Http\Requests\ModuleDtlRequest;

class ModuleController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $getAll = ModuleDtl::get()->toArray();

        return view('ed.modules.modules_results', compact('getAll'));
    }

    public function create(Request $request) {

        return view('ed.modules.modules_create_form');
    }

    public function store(ModuleDtlRequest $request) {

        $data = $request->all();

        $form_data['module_code'] = generate_id('module_dtl', 'module_code', 'MOD', 6);

        $form_data['module_name'] = $data['module_name'];

        $form_data['no_of_credit'] = $data['noOfCredits'];

        $form_data['repeat_price'] = $data['repeat_cost'];

        ModuleDtl::create($form_data);

        return Redirect('module_result');
    }

    public function moduleForDetails(Request $request) {

        $data['department'] = $request->department_val;

        $data['course'] = $request->course_val;
    }

}
