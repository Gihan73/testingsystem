<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UserRegistrationRequest;
use App\Models\User;
use App\Models\Registration;
use App\Models\Lecturer;
use App\Models\Examinar;

class UserRegistrationController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index(Request $request) {

        $category_userRole = explode("_", $request->segment(1))[0];
        //LEC from LEC_user_data 

        $data = $this->formDynamiData($category_userRole);

        switch ($category_userRole) {

            case 'LEC':

                $getAll = Lecturer::get()->toArray();

                break;

            case 'ED':

                $getAll = Examinar::get()->toArray();

                break;

            case 'ADMIN':
                # code...
                break;
        }

        return view('admin.OtherUserRegistration.user_reg_result', compact('getAll', 'data'));
    }

    public function create(Request $request) {

        $category_userRole = explode("_", $request->segment(1))[0];

        $data = $this->formDynamiData($category_userRole);

        return view('admin.OtherUserRegistration.user_reg_form', compact('data'));
    }

    public function formDynamiData($category_userRole) {

        switch ($category_userRole) {

            case 'LEC':

                $data['label'] = 'Lecturers';

                $data['form_url'] = "/LEC_user_create";

                $data['store_url'] = "/LEC_user_store";

                break;

            case 'ED':

                $data['label'] = 'Examination Devisioners';

                $data['form_url'] = "/ED_user_create";

                $data['store_url'] = "/ED_user_store";

                break;

            case 'ADMIN':

                $data['label'] = 'Admin';

                $data['form_url'] = "/ADMIN_user_create";

                $data['store_url'] = "/ADMIN_user_store";

                break;
        }

        return $data;
    }

    public function store(UserRegistrationRequest $request) {

        $data = $request->all();

        $category_userRole = explode("_", $request->segment(1))[0];

        switch ($category_userRole) {

            case 'LEC':

                $reg_no = generate_id('lecturer', 'reg_no', 'LEC', 6);

                break;

            case 'ED':

                $reg_no = generate_id('examinor', 'reg_no', 'ED', 6);

                break;

            case 'ADMIN':

                // $reg_no = generate_id('examinar', 'reg_no', 'ED', 6);

                break;
        }

        $reg_save['reg_no'] = $reg_no;

        $reg_save['fname'] = $data['fname'];

        $reg_save['lname'] = $data['lname'];

        $reg_save['username'] = $data['username'];

        $reg_save['nic'] = $data['nic'];

        $reg_save['address'] = $data['address'];

        $reg_save['mobile'] = $data['mobile'];

        $reg_save['email'] = $data['email'];

        $reg_save['active_status'] = '1';


        // $userrole   =   explode("_",$request->segment(1) )[0];

        $user_save['name'] = $data['username'];

        $user_save['email'] = $data['email'];

        $user_save['userrole'] = $category_userRole;

        $user_save['password'] = Hash::make($data['password']);

        Registration::create($reg_save);

        User::create($user_save);

        return Redirect($category_userRole . ('_user'));
    }

}
