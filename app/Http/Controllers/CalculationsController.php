<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mark;
use App\Models\ModulesToEnroll;
use App\Models\Student;
use App\Models\ModuleDtl;
use App\Models\stuGpaSummary;
use App\Models\StudentGPA;
use App\Models\Courses;
use Auth;

class CalculationsController extends Controller
{

	public function __construct() {

        $this->middleware('auth');
    }
    
    public function generateFinalMarks(Request $request){

    	$year 	= $request->year;

    	$getAll = Mark::getAllMarksToCalc($year);

    	if($getAll){

    		$results = $getAll->toArray();

    		foreach ($results as $key => $result) {
    			
    			$module_dtl = ModulesToEnroll::find($result['enroll_id'])->toArray();
    				
    			$calc_final = $this->calculateFinal($module_dtl,$result);

    		}
    		

    		$final_results = Mark::getAllMarksToCalc($year)->toArray();

    		foreach ($final_results as $key => $f_result) {
   			
    			switch ($f_result) {


    				case ($f_result['continuous_assignment_mark'] == null):

    					$f_result['status'] = 'ANC';

    					break;


    				case ($f_result['exam_mark'] == null):

    					$f_result['status'] = 'AB';
    					
    					break;


    				case (($f_result['continuous_assignment_mark'] != null)  && ($f_result['exam_mark'] != null)):

    					switch ($f_result) {


    						case ($f_result['ﬁnal_mark'] >= 85):

    							$f_result['grade'] = 'A+';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 70):

    							$f_result['grade'] = 'A';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 65):

    							$f_result['grade'] = 'A-';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 60):

    							$f_result['grade'] = 'B+';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 55):

    							$f_result['grade'] = 'B';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 50):

    							$f_result['grade'] = 'B-';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 45):

    							$f_result['grade'] = 'C+';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 40):

    							$f_result['grade'] = 'C';

    							$f_result['status']= 'C';

    							break;


    						case ($f_result['ﬁnal_mark'] >= 35):

    							$f_result['grade'] = 'C-';

    							$f_result['status']= 'C';

    							break;

    						case ($f_result['ﬁnal_mark'] >= 30):

    							$f_result['grade'] = 'D+';

                                $f_result['status']= 'R';

    							break;

    						case ($f_result['ﬁnal_mark'] >= 25):

    							$f_result['grade'] = 'D';

                                $f_result['status']= 'R';

    							break;

    						case ($f_result['ﬁnal_mark'] >= 0):

    							$f_result['grade'] = 'E';
                                // $f_result['grade'] = 'F';
    							$f_result['status']= 'R';

    							break;
    					}

    					break;
    				
    				default:
    					$f_result['status'] = 'N';
    					break;
    			}
    		}


    	}

    	$update = Mark::updateMarks($f_result);

    	if($update){

    		return 'true';

    	}else{

    		return 'false';
    	}

    }

    public function calculateFinal($module_dtl,$result){


    	$cont_assign = ($result['continuous_assignment_mark'] != null)? $result['continuous_assignment_mark'] : 0 ;

    	$writn_exm   = ($result['exam_mark'] != null)? $result['exam_mark'] : 0 ;

    	$result['ﬁnal_mark'] = ($cont_assign/100)*$module_dtl['continuous_assginment_ratio'] + ($writn_exm/100)*$module_dtl['exam_mark_ratio'];    	

    	return Mark::updateMarks($result);

    }


    public function semesterGPACalculate(){

        $active_students = Student::getAllActiveStudents()->toArray();

        $total_credit = 0;

        foreach ($active_students as $key => $student) {
            
            $f_data['current_year']      = $student['current_year'];

            $f_data['current_semester']  = $student['current_semester'];

            $assign_modules_to_sem       = ModulesToEnroll::getAllSubjects($f_data)->get()->toArray(); 

            $assign_enrollment_list      = array_column($assign_modules_to_sem  , 'enroll_id');

            $raw_data['reg_no']          = $student['reg_no'];

            $raw_data['enroll_id']       = $assign_enrollment_list;



            $student_reg_enrols         = Mark::studentEnrollment($raw_data)->with('moduleToEnroll')->get()->toArray();

            $notcomplet_enrollment      = Mark::studentNotCompletedEnrollment($raw_data)->with('moduleToEnroll')->get()->count(); 
            
            if($notcomplet_enrollment == 0){


                $weighted_gpa = 0;


                foreach ($student_reg_enrols as $key => $enrol) {


                    $weight = 0;

                    $grade       = $enrol['grade'];

                    $grade_scale = $this->gradeScaleCalc($enrol['grade']);

                    $module_code = $enrol['module_to_enroll']['module_code'];

                    $credits     = ModuleDtl::find($module_code)->first()->toArray()['no_of_credit'];

                    $weight      = $grade_scale  * $credits;

                    $weighted_gpa= $weighted_gpa + $weight;

                    $total_credit= $total_credit + $credits;

                }

                $calcGPA = $weighted_gpa / $total_credit;

                if($calcGPA != 0){

                    StudentGPA::updateOrCreate([
                        'reg_no'    => $student['reg_no'],

                        'semester'  => $student['current_semester'],
                    ],[
                        'gpa'       => $calcGPA,
                    ]);
                }               

            }

        }
        return 'true';

    }

    public function gradeScaleCalc($grade){

        switch ($grade) {

            case 'A+':
                $grade_scale = 4;
                break;

            case 'A':
                $grade_scale = 4;
                break;

            case 'A-':
                $grade_scale = 3.7;
                break;


            case 'B+':
                $grade_scale = 3.3;
                break;

            case 'B':
                $grade_scale = 3;
                break;

            case 'B-':
                $grade_scale = 2.7;
                break;

            case 'C+':
                $grade_scale = 2.3;
                break;


            case 'C':
                $grade_scale = 2;
                break;

            case 'C-':
                $grade_scale = 1.7;
                break;

            case 'D+':
                $grade_scale = 1.3;
                break;

            case 'D':
                $grade_scale = 1;                                                                                          
                break;

            case 'E':
                $grade_scale = 0;
                break;

            default:
                $grade_scale = 0;
                break;
        }

        return $grade_scale;
    }


    public function overallGPACalculate(){

        $active_students = Student::getAllActiveStudents()->toArray();

        $count =0;


        foreach ($active_students as $key => $student) {

            $raw_data['reg_no']          = $student['reg_no'];

            $raw_data['course_duration'] = Courses::find($student['course'])->toArray()['duration'];

            $count        = StudentGPA::findByStuID($raw_data)->count();

            if($count == $raw_data['course_duration']){

                $sum = StudentGPA::getSumOfGPA($student['reg_no']);

                    $f_data['reg_no'] = $student['reg_no'];

                    $f_data['overall_gpa'] = $sum / $count;

                    $f_data['complete_status'] = '1';

                    stuGpaSummary::updateSummary($f_data);

                    return 'true';

            }

 

        }

        return 'true';
    }


}
