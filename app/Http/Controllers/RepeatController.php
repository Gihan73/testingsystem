<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Mark;
use App\Models\TemporyRepeatEnroll;
use App\Models\ModulesToEnroll;
use App\Models\RepeatApply;
use App\Models\RepeatModule;
use App\Models\moduleDescription;

use Auth;
use Carbon\Carbon;

use App\Http\Requests\RepeatApplylRequest;

class RepeatController extends Controller
{
    
    public function __construct() {

        $this->middleware('auth');
    }

    public function index(){

        return view('student.repeat_modules.repeat_page');
    }

    public function getRepeatList(){

        $results['pending_repeats'] = array();
        $results['tempory_repeats'] = array();

        
        $student_all_repeats = $this->getStudentAllRepeat();

        $results = $this->loadRepeats($student_all_repeats);

        return view('student.repeat_modules.repeat_res_table', compact('results'));
    	
    }

    public function enrollRepeatSubjects(Request $request){

        $enroll_id = $request->enroll_id;

        $tempory_record['reg_no'] = logged_user_dtl(Auth::user()->email)->reg_no;

        $tempory_record['enroll_id'] = $enroll_id;

        TemporyRepeatEnroll::create($tempory_record);

        $results = $this->loadRepeats();

        return view('student.repeat_modules.repeat_res_table', compact('results'));
    }

    public function unenrollRepeatSubjects(Request $request){

        $enroll_id = $request->enroll_id;

        $delete_record['reg_no'] = logged_user_dtl(Auth::user()->email)->reg_no;

        $delete_record['enroll_id'] = $enroll_id;

        TemporyRepeatEnroll::unEnrolledSub($delete_record);

        $results = $this->loadRepeats();

        return view('student.repeat_modules.repeat_res_table', compact('results'));
    }

    public function getStudentAllRepeat(){

        $all_repeats    = array();

        $data['reg_no'] = logged_user_dtl(Auth::user()->email)->reg_no;

        $repeat_subjects= Mark::repeatSubjects($data)->get();

        if($repeat_subjects){

            $all_repeats = $repeat_subjects->toArray();
        }

        return $all_repeats;
    }

    public function loadRepeats(){

        $data['reg_no']  = logged_user_dtl(Auth::user()->email)->reg_no;

        $tempory_records = TemporyRepeatEnroll::getStudentEnrolledSubjects($data)->get('enroll_id')->toArray();
        $tempory_codes   = array_column($tempory_records, 'enroll_id');

        $all_records     = Mark::repeatSubjects($data)->get('mark.enroll_id')->toArray();
        $all_codes       = array_column($all_records, 'enroll_id');

        $pending_codes   = array_diff($all_codes, $tempory_codes);

        $result['pending_results'] = ModulesToEnroll::with('moduleDtl')->whereIn('enroll_id', $pending_codes)->get()->toArray();
        $result['tempory_results'] = ModulesToEnroll::with('moduleDtl')->whereIn('enroll_id', $tempory_codes)->get()->toArray();

        $sum = 0;

        foreach ($result['tempory_results'] as $key => $tempory) {

            $sum = $sum + $tempory['module_dtl']['repeat_price'];
    
        }

        $result['sum'] = $sum;

        return $result;

    }

    public function confirmRepeatSubjects(){

        $student_all_repeats = $this->getStudentAllRepeat();

        $results = $this->loadRepeats($student_all_repeats);

        return view('student.repeat_modules.repeat_confirm_frm', compact('results'));

    }

    public function submitRepeatSubjects(RepeatApplylRequest $request){

        $data   = $request->all();

        $reg_no = logged_user_dtl(Auth::user()->email)->reg_no;


        $fileName =  $reg_no.'_'.date('YmdHis').'.'.$data['upload_receipt']->getClientOriginalExtension();

        $image_save = Storage::disk('public')->putFileAs( 'repeatPayments' , $data['upload_receipt'], $fileName);


        $application_id = generate_id('repeat_apply', 'application_id', 'APP', 6);

        $rpt_app['application_id']  = $application_id;

        $rpt_app['reg_no']          = $reg_no;

        $rpt_app['repeat_fee']      = $data['rptfee'];

        $rpt_app['receipt_img']     = $image_save;

        $rpt_app['apply_date']      = Carbon::now()->format('Y-m-d');

        $rpt_app['approved_date']   = '2000-01-01';
        // $rpt_app['approved_date']    = '0000-00-00';

        RepeatApply::create($rpt_app);


        $delete_record['reg_no']        = $reg_no;

        $allSubs = TemporyRepeatEnroll::selectData($delete_record)->toArray();

        TemporyRepeatEnroll::deleteFromTempory($delete_record);

        foreach ($allSubs as $key => $subDtl) {
            
            $filtr_mark['reg_no']       = $subDtl['reg_no'];

            $filtr_mark['enroll_id']    = $subDtl['enroll_id'];


            $updt_mrk['exam_mark']          = null;

            $updt_mrk['ﬁnal_mark']          = null;

            $updt_mrk['grade']              = 'N';

            $updt_mrk['status']             = 'N';

            $updt_mrk['approved_status']    = '0';


            $rpt_modules['application_id']  = $application_id;

            $rpt_modules['enroll_id']       = $subDtl['enroll_id'];

            Mark::updateOrCreate($filtr_mark,$updt_mrk);

            RepeatModule::create($rpt_modules);
        }  

        return view('dashboard.stu_dashboard');

    }


    public function pendingRepeatList(){

        $form_dtls = array();

        $pending_receipt = RepeatApply::getPendingReceipts()->with('student')->get();

        if($pending_receipt){

            $form_dtls = $pending_receipt->toArray();
        }

        return view('ed.repeatForms.pnding_repeat_list' ,compact('form_dtls'));

    }

    public function viewRepeatApplication(Request $request){

        $application_id = $request->app_id;

        $appDtl = RepeatApply::with('student')->with('repeatModule')->find($application_id)->toArray();

        foreach ($appDtl['repeat_module'] as $key => $enroll_val) {

            $enroll_id = $enroll_val['enroll_id'];

            $mod_dtl[]   = moduleDescription::find($enroll_id)->toArray();

        }
        $appDtl['repeat_module'] = $mod_dtl;

        return view('ed.repeatForms.view_repeat_application' ,compact('appDtl'));
    }

    public function approveRepeatApplication(Request $request){

        $filter['application_id']       = $request->app_id;

        $apprv_dtl['approved_date']     =   Carbon::now()->format('Y-m-d');

        $apprv_dtl['approved_by']       =   logged_user_dtl(Auth::user()->email)->reg_no;

        $apprv_dtl['approved_status']   =   '1';

        RepeatApply::updateOrCreate($filter,$apprv_dtl);

        return Redirect('pnding_repeat_list');

    }
}
