<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ModuleToEnrollRequest;
use App\Models\ModulesToEnroll;
use App\Models\Department;
use App\Models\Courses;
use App\Models\Lecturer;
use App\Models\ModuleDtl;
use App\Models\StudenDtl;
use App\Models\TemporyEnroll;
use App\Models\Mark;
use App\Models\StuResultSummary;
use Auth;
use Carbon\Carbon;

class ModuleToEnrollController extends Controller {

    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {

        $getAll['modules'] = ModulesToEnroll::with('moduleDtl')->with('department')->with('course')->with('registration')->get()->toArray();

        return view('ed.modules.modules_to_enroll_results', compact('getAll'));
    }

    public function create() {

        $form_dtls['faculties'] = Department::get()->toArray();

        $form_dtls['courses'] = Courses::get()->toArray();

        $form_dtls['lecturers'] = Lecturer::get()->toArray();

        $form_dtls['modules'] = ModuleDtl::get()->toArray();

        return view('ed.modules.enroll_assign_form', compact('form_dtls'));
    }

    public function store(ModuleToEnrollRequest $request) {

        $form_data = $request->all();
        $enroll_id = generate_id('modules_to_enroll', 'enroll_id', 'SUB', 6);
        $data['enroll_id'] = $enroll_id;
        $data['department'] = $form_data['department'];
        $data['course'] = $form_data['course'];
        $data['course_year'] = $form_data['course_year'];
        $data['semester'] = $form_data['semester'];
        $data['module_code'] = $form_data['module_name'];
        $data['lecture_id'] = $form_data['lecturer'];
        $data['continuous_assginment_ratio'] = $form_data['assign_markr'];
        $data['exam_mark_ratio'] = 100 - $form_data['assign_markr'];

        ModulesToEnroll::create($data);

        return Redirect('module_to_enroll_result');
    }

    public function enrollMySubjects(Request $request) {

        return view('student.enroll_modules.module_results');
    }

    public function getAllSubjects(Request $request) {

        $result['pending_results'] = array();
        $result['tempory_results'] = array();


        $student_acedemic = $this->getStudentAcadamicDtl();

        $result = $this->loadSubjects($student_acedemic);

        return view('student.enroll_modules.module_res_table', compact('result'));
    }

    public function enrollSubjects(Request $request) {

        $enroll_id = $request->enroll_id;

        $student_acedemic = $this->getStudentAcadamicDtl();

        $tempory_record['reg_no'] = logged_user_dtl(Auth::user()->email)->reg_no;

        $tempory_record['course_year'] = $student_acedemic['current_year'];

        $tempory_record['course_semester'] = $student_acedemic['current_semester'];

        $tempory_record['enroll_id'] = $enroll_id;

        TemporyEnroll::create($tempory_record);


        $result = $this->loadSubjects($student_acedemic);

        return view('student.enroll_modules.module_res_table', compact('result'));
    }

    public function unEnrollSubjects(Request $request){

        $enroll_id = $request->enroll_id;
        $student_acedemic = $this->getStudentAcadamicDtl();
        $delete_record['reg_no'] = logged_user_dtl(Auth::user()->email)->reg_no;
        $delete_record['course_year'] = $student_acedemic['current_year'];
        $delete_record['course_semester'] = $student_acedemic['current_semester'];
        $delete_record['enroll_id'] = $enroll_id;
        TemporyEnroll::unEnrolledSub($delete_record);
        $result = $this->loadSubjects($student_acedemic);
        return view('student.enroll_modules.module_res_table', compact('result'));

    }

    public function getStudentAcadamicDtl(){

        $logged_user_email = Auth::user()->email;
        $reg_no = logged_user_dtl($logged_user_email)->reg_no;
        $student_acedemic = StudenDtl::getStudentAcademic($reg_no)->toArray();
        return $student_acedemic;
    }

    public function loadSubjects($student_acedemic){

        $tempory_records = TemporyEnroll::getStudentEnrolledSubjects($student_acedemic)->get('enroll_id')->toArray();
        $tempory_codes = array_column($tempory_records, 'enroll_id');

        $all_records = ModulesToEnroll::getAllSubjects($student_acedemic)->get('enroll_id')->toArray();
        $all_codes = array_column($all_records, 'enroll_id');

        $pending_codes = array_diff($all_codes, $tempory_codes);

        $result['pending_results'] = ModulesToEnroll::with('moduleDtl')->with('registration')->whereIn('enroll_id', $pending_codes)->get()->toArray();
        $result['tempory_results'] = ModulesToEnroll::with('moduleDtl')->with('registration')->whereIn('enroll_id', $tempory_codes)->get()->toArray();

        return $result;
    }

    public function submitSubjects(Request $request){

        $student_acedemic = $this->getStudentAcadamicDtl();

        $reg_no = logged_user_dtl(Auth::user()->email)->reg_no;


        $delete_record['reg_no']        = $reg_no;

        $delete_record['course_year']   = $student_acedemic['current_year'];

        $delete_record['course_semester'] = $student_acedemic['current_semester'];

        $allSubs = TemporyEnroll::selectData($delete_record)->toArray();


        TemporyEnroll::deleteFromTempory($delete_record);

        foreach ($allSubs as $key => $subDtl) {

            $r_mark['reg_no']       = $subDtl['reg_no'];

            $r_mark['enroll_id']    = $subDtl['enroll_id'];

            $r_mark['current_year'] = $subDtl['course_year'];

            $r_mark['year']         = Carbon::now()->format('Y');

            Mark::create($r_mark);
        }

        // $summary_data['reg_no'] = $reg_no;

        // StuResultSummary::create($summary_data);

        return view('student.enroll_modules.module_results');

    }

    public function moduleForCourse(Request $request){

        $details = array();

        $data['department'] = $request->department;

        $data['course']     = $request->course_id;

        $module_list = ModulesToEnroll::getAllSubjects($data)->with('moduleDtl')->get();

        if ($module_list) {

            $modules = $module_list->toArray();

            foreach ($modules as $key => $mod_dtl) {

                $details[] = $mod_dtl['module_dtl'];

            }

            return array_column($details, 'module_name', 'module_code');

        } else {

            return false;
        }
    }

}
