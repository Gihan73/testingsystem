<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\AttitudeTestRequest;
use App\Models\Department;
use App\Models\Courses;
use App\Models\ModuleDtl;
use App\Models\ModulesToEnroll;
use App\Models\ApptituteMark;
use App\Models\Mark;
use App\Imports\ContinuousAssignmentMarkImport;
use App\Imports\AttitudeTestMarks;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class AttitudeTest extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $form_dtls['department']    = Department::get()->toArray();
        $form_dtls['courses']       = Courses::get()->toArray();

        return view('admin.attitudeTest.attitudeTestUpload', compact('form_dtls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAttituteTestMarks(AttitudeTestRequest $request)
    {
        $data = $request->all();
        // var_dump($data);
        // $data['module_code'] = $data['module_name'];

        $fileName =  $data['department'].'_'.$data['course'].'_'.date('YmdHis').'.'.$data['inputFile']->getClientOriginalExtension();

        $image_save = Storage::disk('public')->putFileAs( 'attitudeTest' , $data['inputFile'], $fileName);

        
        $ex_data = Excel::toArray(new AttitudeTestMarks, request()->file('inputFile')); 

        foreach ($ex_data as $key => $tab) {
            foreach ($tab as $key => $value) {
                $value['department']         =  $data['department'];
                $value['course']   =  $data['course'];
                $tab_s[] = $value;
            } 
            $data_s[] = $tab_s;
        }
        collect(head($data_s))

        ->each(function ($row, $key) {
           
            ApptituteMark::insertMarks($row);
        });

        // echo "<pre>";print_r($data);print_r($fileName); echo "</pre>";die('end');
        return Redirect('attitudeTestView');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
