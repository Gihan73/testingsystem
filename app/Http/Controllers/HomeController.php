<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $user_role = Auth::user()->userrole;

        switch ($user_role) {

            case 'STU':

                return view('dashboard.stu_dashboard');

                break;

            case 'LEC':

                return view('dashboard.lec_dashboard');

                break;

            case 'ED':

                return view('dashboard.ed_dashboard');

                break;

            case 'ADMIN':

                return view('dashboard.admin_dashboard');

                break;
        }
    }

    public function profile() {

        $user_role = Auth::user()->userrole;


        switch ($user_role) {

            case 'STU':

                $data['extend_blade'] = 'dashboard.stu_dashboard';

                $data['label'] = 'Student';

                break;

            case 'LEC':

                $data['extend_blade'] = 'dashboard.lec_dashboard';

                $data['label'] = 'Lecturer';

                break;

            case 'ED':

                $data['extend_blade'] = 'dashboard.ed_dashboard';

                $data['label'] = 'ED';

                break;

            case 'ADMIN':

                $data['extend_blade'] = 'dashboard.admin_dashboard';

                $data['label'] = 'Admin';

                break;
        }

        return view('profile.profile', compact('data'));
    }

}
