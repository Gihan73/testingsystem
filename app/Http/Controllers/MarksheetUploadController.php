<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\MarkSheetUploadRequest;
use App\Models\Department;
use App\Models\Courses;
use App\Models\ModuleDtl;
use App\Models\ModulesToEnroll;
use App\Models\UploadedMarksheets;
use App\Models\Mark;
use App\Imports\ContinuousAssignmentMarkImport;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class MarksheetUploadController extends Controller
{

	public function __construct() {
        $this->middleware('auth');
    }
    
    public function marksheetForm(){

    	$form_dtls['department'] 	= Department::get()->toArray();
        $form_dtls['courses'] 		= Courses::get()->toArray();
        $form_dtls['modules'] 		= ModuleDtl::get()->toArray();

    	return view('lecturer.marksheetUpload.uploadForm', compact('form_dtls'));
    }

    public function storeMarksheet(MarkSheetUploadRequest $request){

    	$data = $request->all();

    	$data['module_code'] = $data['module_name'];

    	$fileName =  $data['department'].'_'.$data['course'].'_'.$data['module_name'].'_'.date('YmdHis').'.'.$data['inputFile']->getClientOriginalExtension();

    	$image_save = Storage::disk('public')->putFileAs( 'markSheets' , $data['inputFile'], $fileName);

    	$enroll_id  = ModulesToEnroll::getAllSubjects($data)->first('enroll_id')->toArray()['enroll_id'];

    	$uploadDoc['enroll_id'] 	= $enroll_id;
        $uploadDoc['document_path'] = $image_save;
    	$uploadDoc['semester_yr']   = $data['semester_yr'];
    	$uploadDoc['created_by'] 	= logged_user_dtl(Auth::user()->email)->reg_no;
    	$uploadDoc['approved_by'] 	= '';
    	$uploadDoc['approved_at'] 	= '2000-01-01';
    	// $uploadDoc['approved_at'] 	= '0000-00-00';
    	UploadedMarksheets::create($uploadDoc);
        $ex_data = Excel::toArray(new ContinuousAssignmentMarkImport, request()->file('inputFile')); 

        foreach ($ex_data as $key => $tab) {
            foreach ($tab as $key => $value) {
                $value['enroll_id']         =  $enroll_id;
                $value['approved_status']   =  '0';
                $value['year']      =  $data['semester_yr'];
                $tab_s[] = $value;
            } 
            $data_s[] = $tab_s;
        }
        collect(head($data_s))

        ->each(function ($row, $key) {
            
            Mark::updateMarks($row);
        });

        // echo "<pre>";print_r($data);print_r($fileName); echo "</pre>";die('end');
        return Redirect('upload_form');

    }

    public function pendingMarksheetList(){

        $results = UploadedMarksheets::listAllPendingSheets()->get();

        if($results){

            $form_dtls = $results->toArray();

            return view('ed.markSheets.pnding_mrksht_list', compact('form_dtls'));
        }

    }

    public function readNApproveMarks(Request $request){

        $file_name = $request->file_name;


        // echo "<pre>";print_r($file_name); echo "</pre>";die('end');

        $data['department'] = explode("_", $file_name)[0];

        $data['course']     = explode("_", $file_name)[1];

        $data['module_code']= explode("_", $file_name)[2];

        $dtl['enroll_id']   = ModulesToEnroll::getAllSubjects($data)->first()->toArray();


        $dtl['semester_yr'] = $request->semester_yr;

        $dtl['approved_status'] = '1';

        $dtl['approved_by'] = logged_user_dtl(Auth::user()->email)->reg_no;

        $dtl['approved_at'] = Carbon::now()->format('Y-m-d');

        UploadedMarksheets::approveDoc($dtl);

        Mark::approveMark($dtl);

        return 'true';

        // return Redirect('pnding_mrksht_list');
    }

}
