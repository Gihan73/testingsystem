<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StudentRegistrationRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\Registration;
use App\Models\User;
use App\Models\StudenDtl;
use App\Models\Student;
use App\Models\Department;
use App\Models\Courses;
use Carbon\Carbon;

class StudentRegistrationController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $getAll = Student::get()->toArray();

        return view('admin.StudentRegistration.stu_reg_result', compact('getAll'));
    }

    public function create() {

        $form_dtls['faculties'] = Department::get()->toArray();

        $form_dtls['courses'] = Courses::get()->toArray();

        return view('admin.StudentRegistration.stu_reg_form', compact('form_dtls'));
    }

    public function store(StudentRegistrationRequest $request) {

        $data = $request->all();

        $dateFormat = new Carbon();

        $reg_no = generate_id('student', 'reg_no', 'REGST', 6);


        $reg_save['reg_no'] = $reg_no;

        $reg_save['fname'] = $data['fname'];

        $reg_save['lname'] = $data['lname'];

        $reg_save['username'] = $data['username'];

        $reg_save['nic'] = $data['nic'];

        $reg_save['address'] = $data['address'];

        $reg_save['mobile'] = $data['mobile'];

        $reg_save['email'] = $data['email'];

        $reg_save['active_status'] = '1';


        $user_save['name'] = $data['username'];

        $user_save['email'] = $data['email'];

        $user_save['userrole'] = 'STU';

        $user_save['password'] = Hash::make($data['password']);


        $studtl_save['reg_no'] = $reg_no;

        $studtl_save['department'] = $data['department'];

        $studtl_save['course'] = $data['course'];

        $studtl_save['started_date'] = $dateFormat->parse($data['started_date'])->format('Y-m-d');

        $studtl_save['is_apptitude_student'] = (isset($data['apptituter'])) ? '1' : '0';

        $studtl_save['current_year'] = 1; //current course year

        $studtl_save['current_semester'] = 1;

        Registration::create($reg_save);

        User::create($user_save);

        StudenDtl::create($studtl_save);

        return Redirect('stu_user');
    }

}
