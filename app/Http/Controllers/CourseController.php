<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Courses;

class CourseController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function courseForFaculty($department_code) {

        $curs = Courses::courseForFaculty($department_code);

        if ($curs) {

            $course_list = $curs->toArray();

            return array_column($course_list, 'course_name', 'course_code');
        } else {

            return false;
        }
    }

}
