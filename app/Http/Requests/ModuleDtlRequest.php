<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\validation\Rule;

class ModuleDtlRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'module_name' => 'required',
            'noOfCredits' => 'required|numeric',
            'repeat_cost' => ['required', 'numeric', Rule::notIn(['0'])]
        ];
    }

}
