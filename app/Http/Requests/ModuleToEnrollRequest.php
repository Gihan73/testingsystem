<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\validation\Rule;

class ModuleToEnrollRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        if ($this->isMethod('post')) {

            return $this->postRules();
        }
    }

    public function postRules() {


        return [
            'module_name' => 'required',
            'department' => 'required',
            'course' => 'required',
            'course_year' => ['required', 'integer', Rule::notIn(['0'])],
            'semester' => ['required', Rule::notIn(['0'])],
            'lecturer' => 'required',
            'assign_markr' => 'required|integer|between:1,100',
        ];
    }

}
