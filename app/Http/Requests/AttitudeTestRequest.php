<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttitudeTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'department' => 'required',
            'course' => 'required',
            'inputFile' => 'required|mimes:csv,xlsx',
        ];
    }
}
