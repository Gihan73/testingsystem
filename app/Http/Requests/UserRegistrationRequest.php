<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegistrationRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        if ($this->isMethod('post')) {

            return $this->postRules();
        }
    }

    public function postRules() {

        return [
            'fname' => 'required',
            'lname' => 'required',
            'nic' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'mobile' => 'required| max:10',
            'username' => 'required',
            'password' => 'required_with:password_confirm|same:password_confirm',
        ];
    }

}
