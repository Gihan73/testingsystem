<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model {

    protected $table = "lecturer";
    protected $primaryKey = 'reg_no';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

}
