<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModuleDtl extends Model {

    protected $table = "module_dtl";
    protected $primaryKey = 'module_code';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = true;
    protected $fillable = [
        'module_code',
        'module_name',
        'no_of_credit',
        'repeat_price',
    ];

    public function modulesToEnroll() {

        return $this->hasMany('App\Models\ModulesToEnroll', 'module_code', 'module_code');
    }

}
