<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepeatApply extends Model {
    
    protected $table = "repeat_apply";
    protected $primaryKey = 'application_id';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = true;
    protected $fillable = [
        'application_id',
        'reg_no',
        'repeat_fee',
        'receipt_img',
        'apply_date',
        'approved_date',
        'approved_by',
        'approved_status'
    ];

    public function registration() {

        return $this->belongsTo('App\Models\Registration', 'reg_no');
    }

    public function student() {

        return $this->belongsTo('App\Models\Student', 'reg_no');
    }

    public function repeatModule() {

        return $this->hasMany('App\Models\RepeatModule', 'application_id');
    }

    protected function getPendingReceipts(){

        return self::where('approved_status','0');
    }
}
