<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StuResultSummary extends Model {
    
    protected $table = "stu_result_summary";
    protected $primaryKey = 'rowno';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'rowno',
        'reg_no',
        'sem_1_gpa',
        'sem_2_gpa',
        'sem_3_gpa',
        'sem_4_gpa',
        'sem_5_gpa',
        'sem_6_gpa',
        'overall_gpa',
        'class',
        'complete_status',
    ];

    

}
