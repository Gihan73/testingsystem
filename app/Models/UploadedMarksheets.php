<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UploadedMarksheets extends Model
{
    protected $table = "uploaded_marksheets";
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'id',
        'enroll_id',
        'document_path',
        'semester_yr',
        'created_by',
        'approved_by',
        'approved_at',
    ];

    protected function listAllPendingSheets(){

        return self::where('approved_by' , '=' ,' ');
        // ->where('approved_at' , '0000-00-00')
    }

    protected function approveDoc($data){

        return self::where('enroll_id' , $data['enroll_id']['enroll_id'] )
        ->where('semester_yr', $data['semester_yr'])
        ->update(['approved_by' => $data['approved_by'] , 'approved_at' => $data['approved_at'] ]);
    }
}
