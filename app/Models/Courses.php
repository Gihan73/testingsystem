<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model {

    protected $table = "courses";
    protected $primaryKey = 'course_code';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = true;
    protected $fillable = [
        'course_code',
        'course_name',
        'department_code'
    ];

    public function modulesToEnroll() {

        return $this->hasMany('App\Models\ModulesToEnroll', 'course');
    }

    protected function courseForFaculty($department_code) {

        return self::where('department_code', $department_code)->get(['course_code', 'course_name']);
    }

}
