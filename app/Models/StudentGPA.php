<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentGPA extends Model
{
    
    protected $table = "student_g_p_a_s";
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'id',
        'reg_no',
        'semester',
        'gpa'
    ];

    protected function findByStuID($data){
    
        return self::where('reg_no', $data['reg_no'] );
    }


    protected function getSumOfGPA($reg_no){

        return self::where('reg_no',$reg_no)
        ->sum('gpa');
    }

}
