<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporyEnroll extends Model {

    protected $table = "tempory_enrolls";
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'id',
        'reg_no',
        'department',
        'course_year',
        'course_semester',
        'enroll_id'
    ];

    protected function selectData($data){

        return self::when((isset($data['reg_no']) && $data['reg_no'] != null), function ($query) use ($data){
            return $query->where('reg_no','=',$data['reg_no']);
        })
        ->when((isset($data['course_year']) && $data['course_year'] != null), function ($query) use ($data){
            return $query->where('course_year','=',$data['course_year']);
        })
        ->when((isset($data['course_semester']) && $data['course_semester'] != null), function ($query) use ($data){
            return $query->where('course_semester','=',$data['course_semester']);
        })
        ->when((isset($data['enroll_id']) && $data['enroll_id'] != null), function ($query) use ($data){
            return $query->where('enroll_id','=',$data['enroll_id']);
        })
        ->get();
    }


    protected function getStudentEnrolledSubjects($data) {

        return self::where('reg_no', $data['reg_no'])
                        ->where('course_year', $data['current_year'])
                        ->where('course_semester', $data['current_semester']);
    }

    protected function unEnrolledSub($data){

        return self::where('reg_no', $data['reg_no'])
        ->where('course_year',$data['course_year'])
        ->where('course_semester',$data['course_semester'])
        ->where('enroll_id',$data['enroll_id'])
        ->delete();
    }

    protected function deleteFromTempory($data){

        return self::where('reg_no', $data['reg_no'])
        ->where('course_year',$data['course_year'])
        ->where('course_semester',$data['course_semester'])
        ->delete();
    }

}
