<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mark extends Model {
    
    protected $table = "mark";
    protected $primaryKey = 'mark_rowno';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'mark_rowno',
        'reg_no',
        'enroll_id',
        'current_year',
        'year',
        'continuous_assignment_mark',
        'exam_mark',
        'ﬁnal_mark',
        'grade',
        'status',
        'approved_status',
    ];

    public function moduleToEnroll(){

        return $this->belongsTo('App\Models\ModulesToEnroll', 'enroll_id');
    }

    protected function updateMarks($data){

        return  self::where('reg_no',$data['reg_no'])
        ->where('enroll_id',$data['enroll_id'])
        ->where('year',$data['year'])
        ->update($data);
        // ->update(['continuous_assignment_mark' => $data['continuous_assignment_mark']]);
    }

    protected function approveMark($data){

        return self::where('enroll_id' , $data['enroll_id']['enroll_id'] )
        ->where('year', $data['semester_yr'])
        ->update(['approved_status' =>  $data['approved_status'] ]);
    }

    protected function getAllMarksToCalc($year){

        return self::where('year',$year)
        ->where('approved_status','1')
        ->get();
    }

    protected function studentEnrollment($data){

        return self::where('reg_no',$data['reg_no'])
        ->where('approved_status','1')
        ->whereIn('enroll_id', $data['enroll_id']);

    }

    protected function studentNotCompletedEnrollment($data){

        return self::where('reg_no',$data['reg_no'])
        ->where('status', '!=','C')
        ->whereIn('enroll_id', $data['enroll_id']);

    }

    protected function repeatSubjects($data){

        return self::join('modules_to_enroll', 'modules_to_enroll.enroll_id','=','mark.enroll_id')
        ->join('module_dtl', 'module_dtl.module_code','=','modules_to_enroll.module_code')
        ->where('mark.reg_no',$data['reg_no'])
        ->where('status', 'R');
    }
}
