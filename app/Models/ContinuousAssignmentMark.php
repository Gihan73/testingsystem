<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContinuousAssignmentMark extends Model {
    
    protected $table = "continuous_assignment_mark";
    protected $primaryKey = 'rowno';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'rowno',
        'reg_no',
        'mark_rowno',
        'current_date',
        'assignment_type',
        'assignment_mark',
    ];
}
