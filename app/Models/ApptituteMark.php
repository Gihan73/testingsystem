<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApptituteMark extends Model {
    
    protected $table = "aptitude_mark";
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'id',
        'nic',
        'name',
        'mark',
        'date',
        'email',
        'department',
        'course',
        'reg_status',
        'email_send_on',
    ];

    public function department() {

        return $this->belongsTo('App\Models\Department', 'department');
    }

    public function course() {

        return $this->belongsTo('App\Models\Courses', 'course');
    }

    protected function insertMarks($data){
// dd($data);
    	// $data['reg_no'];
    	return self::create($data);
        // return  self::where('reg_no',$data['register_number'])
        // ->update($data);
        // ->update(['continuous_assignment_mark' => $data['continuous_assignment_mark']]);
    }
}
