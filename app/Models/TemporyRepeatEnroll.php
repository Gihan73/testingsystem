<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TemporyRepeatEnroll extends Model
{
    protected $table = "tempory_repeat_enrolls";
    protected $primaryKey = 'id';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'id',
        'reg_no',
        'enroll_id'
    ];

    protected function getStudentEnrolledSubjects($data) {

        return self::where('reg_no', $data['reg_no']);
    }

    protected function unEnrolledSub($data){

        return self::where('reg_no', $data['reg_no'])
        ->where('enroll_id',$data['enroll_id'])
        ->delete();
    }

    protected function selectData($data){

        return self::when((isset($data['reg_no']) && $data['reg_no'] != null), function ($query) use ($data){
            return $query->where('reg_no','=',$data['reg_no']);
        })
        ->when((isset($data['enroll_id']) && $data['enroll_id'] != null), function ($query) use ($data){
            return $query->where('enroll_id','=',$data['enroll_id']);
        })
        ->get();
    }

    protected function deleteFromTempory($data){

        return self::where('reg_no', $data['reg_no'])
        ->delete();
    }
}
