<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RepeatModule extends Model {
    
    protected $table = "repeat_module";
    protected $primaryKey = 'row_no';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'row_no',
        'application_id',
        'enroll_id'
    ];

    public function application() {

        return $this->belongsTo('App\Models\RepeatApply', 'application_id');
    }

    public function enrollDetails() {

        return $this->belongsTo('App\Models\moduleDescription', 'enroll_id');
    }
}
