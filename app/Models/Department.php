<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model {

    protected $table = "departments";
    protected $primaryKey = 'department_code';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = true;
    protected $fillable = [
        'department_code',
        'department_name'
    ];

    public function modulesToEnroll() {

        return $this->hasMany('App\Models\ModulesToEnroll', 'department');
    }

}
