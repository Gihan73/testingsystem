<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class stuGpaSummary extends Model
{
    
    protected $table = "stu_gpa_summary";
    protected $primaryKey = 'rowno';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'rowno',
        'reg_no',
        'overall_gpa',
        'class',
        'complete_status',
    ];


    protected function updateSummary($data){

        return self::where('reg_no',$data['reg_no'])
        ->update($data);
    }

}
