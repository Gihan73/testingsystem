<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model {

    protected $table = "student";
    protected $primaryKey = 'reg_no';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    public function repeatApply() {

        return $this->hasMany('App\Models\RepeatApply', 'reg_no');
    }

    protected function getAllActiveStudents(){

    	return self::where('active_status','1')->get();
    }

}
