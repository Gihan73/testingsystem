<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Examinar extends Model {

    protected $table = "examinor";
    protected $primaryKey = 'reg_no';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

}
