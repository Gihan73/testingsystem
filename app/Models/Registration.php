<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model {

    protected $table = "registration";
    protected $primaryKey = 'reg_no';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = true;
    protected $fillable = [
        'reg_no',
        'fname',
        'lname',
        'username',
        'nic',
        'address',
        'mobile',
        'email',
        'active_status'
    ];

    public function modulesToEnroll() {

        return $this->hasMany('App\Models\ModulesToEnroll', 'reg_no');
    }

    public function repeatApply() {

        return $this->hasMany('App\Models\RepeatApply', 'reg_no');
    }

}
