<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudenDtl extends Model {

    protected $table = "studen_dtl";
    protected $primaryKey = 'stu_dtl_id';
    protected $keyType = 'int';
    public $incrementing = true;
    public $timestamps = true;
    protected $fillable = [
        'stu_dtl_id',
        'reg_no',
        'department',
        'course',
        'current_year',
        'current_semester',
        'started_date',
        'end_year',
        'is_apptitude_student'
    ];

    protected function getStudentAcademic($reg_no) {

        return self::where('reg_no', $reg_no)->first();
    }

}
