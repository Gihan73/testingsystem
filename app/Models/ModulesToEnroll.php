<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModulesToEnroll extends Model {

    protected $table = "modules_to_enroll";
    protected $primaryKey = 'enroll_id';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = true;
    protected $fillable = [
        'enroll_id',
        'department',
        'course',
        'course_year',
        'semester',
        'module_code',
        'lecture_id',
        'continuous_assginment_ratio',
        'exam_mark_ratio',
    ];

    public function moduleDtl() {

        return $this->belongsTo('App\Models\ModuleDtl', 'module_code');
    }

    public function department() {

        return $this->belongsTo('App\Models\Department', 'department');
    }

    public function course() {

        return $this->belongsTo('App\Models\Courses', 'course');
    }

    public function registration() {

        return $this->belongsTo('App\Models\Registration', 'lecture_id');
    }

    public function mark() {

        return $this->hasMany('App\Models\Mark', 'enroll_id', 'enroll_id');
    }

    protected function getAllSubjects($data) {

        return self::when((isset($data['department']) && $data['department'] != null), function ($query) use ($data){
            return $query->where('department','=',$data['department']);
        })
        ->when((isset($data['course']) && $data['course'] != null), function ($query) use ($data){
            return $query->where('course','=',$data['course']);
        })
        ->when((isset($data['current_year']) && $data['current_year'] != null), function ($query) use ($data){
            return $query->where('course_year','=',$data['current_year']);
        })
        ->when((isset($data['current_semester']) && $data['current_semester'] != null), function ($query) use ($data){
            return $query->where('semester','=',$data['current_semester']);
        })
        ->when((isset($data['module_code']) && $data['module_code'] != null), function ($query) use ($data){
            return $query->where('module_code','=',$data['module_code']);
        });
    }

}
