<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class moduleDescription extends Model
{
    protected $table = "module_description";
    protected $primaryKey = 'enroll_id';
    protected $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    public function repeatModule() {

        return $this->hasMany('App\Models\RepeatModule', 'enroll_id');
    }
}
