<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepeatAppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repeat_apply', function (Blueprint $table) {
            $table->string('application_id',15);
            $table->string('reg_no', 15)->unique();
            $table->decimal('repeat_fee', 7, 2);
            $table->char('payment_status', 1);
            $table->date('apply_date');
            $table->char('registar_approved',1);
            $table->date('approved_date');
            $table->char('approved_status',1);
            $table->timestamps();
            $table->primary('application_id');
            $table->foreign('reg_no')->references('reg_no')->on('registration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repeat_apply');
    }
}
