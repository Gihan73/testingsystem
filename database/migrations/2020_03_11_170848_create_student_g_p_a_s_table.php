<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentGPASTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_g_p_a_s', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reg_no', 15);
            $table->integer('semester');
            $table->decimal('gpa', 5, 4);
            $table->timestamps();
            $table->foreign('reg_no')->references('reg_no')->on('registration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_g_p_a_s');
    }
}
