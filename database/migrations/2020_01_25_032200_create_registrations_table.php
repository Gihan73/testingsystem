<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration', function (Blueprint $table) {
            $table->string('reg_no', 15);
            $table->string('fname', 30);
            $table->string('lname', 30);
            $table->string('username', 20);
            $table->string('nic', 15)->unique();
            $table->string('address', 200);
            $table->string('mobile', 10);
            $table->string('email', 100)->unique();
            $table->char('active_status', 1);
            $table->timestamps();
            $table->primary('reg_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration');
    }
}
