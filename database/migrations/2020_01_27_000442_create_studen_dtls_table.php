<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudenDtlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studen_dtl', function (Blueprint $table) {
            $table->bigIncrements('stu_dtl_id');
            $table->string('reg_no', 15)->unique();
            $table->string('department', 100);
            $table->string('course', 100);
            $table->date('started_date');
            $table->integer('end_year');
            $table->integer('current_year');
            $table->integer('current_semester');
            $table->char('is_apptitude_student',1);
            $table->timestamps();
            $table->foreign('reg_no')->references('reg_no')->on('registration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studen_dtl');
    }
}
