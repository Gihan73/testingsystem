<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModulesToEnrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules_to_enroll', function (Blueprint $table) {

            $table->string('enroll_id',10);
            $table->string('department',50);
            $table->string('course',200);
            $table->integer('course_year');
            $table->integer('semester');
            $table->string('module_code',10);
            $table->string('lecture_id',5);
            $table->string('continuous_assginment_ratio',7);
            $table->string('exam_mark_ratio',7);
            $table->timestamps();
            $table->primary('enroll_id');
            $table->foreign('module_code')->references('module_code')->on('module_dtl');
            $table->foreign('lecture_id')->references('reg_no')->on('registration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modules_to_enroll');
    }
}
