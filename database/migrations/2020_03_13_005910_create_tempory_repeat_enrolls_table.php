<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemporyRepeatEnrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tempory_repeat_enrolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reg_no', 15);
            $table->string('enroll_id',10);
            $table->timestamps();
            $table->foreign('reg_no')->references('reg_no')->on('registration');
            $table->foreign('enroll_id')->references('enroll_id')->on('modules_to_enroll');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tempory_repeat_enrolls');
    }
}
