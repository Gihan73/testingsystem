<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mark', function (Blueprint $table) {
            $table->bigIncrements('mark_rowno');
            $table->string('reg_no', 15);
            $table->string('enroll_id',10);
            $table->integer('current_year');
            $table->integer('continuous_assignment_mark');
            $table->integer('exam_mark');
            $table->integer('ﬁnal_mark');
            $table->char('grade',1);
            $table->char('status',1);
            $table->timestamps();
            $table->foreign('reg_no')->references('reg_no')->on('registration');
            $table->foreign('enroll_id')->references('enroll_id')->on('modules_to_enroll');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mark');
    }
}
