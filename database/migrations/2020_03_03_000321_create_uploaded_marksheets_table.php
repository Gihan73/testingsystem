<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadedMarksheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaded_marksheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('enroll_id',15);
            $table->string('document_path',200);
            $table->string('created_by',15);
            $table->string('approved_by',15);
            $table->date('approved_at',15);
            $table->timestamps();
            $table->foreign('enroll_id')->references('enroll_id')->on('modules_to_enroll');
            $table->foreign('created_by')->references('reg_no')->on('registration');
            $table->foreign('approved_by')->references('reg_no')->on('registration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploaded_marksheets');
    }
}
