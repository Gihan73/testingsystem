<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModuleDtlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_dtl', function (Blueprint $table) {
            $table->string('module_code',15);
            $table->string('module_name',100);
            $table->integer('no_of_credit');
            $table->double('repeat_price',7,2);
            $table->timestamps();
            $table->primary('module_code');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module_dtl');
    }
}
