<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStuResultSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stu_result_summary', function (Blueprint $table) {
            $table->bigIncrements('rowno');
            $table->string('reg_no', 15)->unique();
            $table->decimal('sem_1_gpa', 5, 4);
            $table->decimal('sem_2_gpa', 5, 4);
            $table->decimal('sem_3_gpa', 5, 4);
            $table->decimal('sem_4_gpa', 5, 4);
            $table->decimal('sem_5_gpa', 5, 4);
            $table->decimal('sem_6_gpa', 5, 4);
            $table->decimal('overall_gpa', 5, 4);
            $table->char('class', 2);
            $table->char('complete_status', 1);
            $table->timestamps();
            $table->foreign('reg_no')->references('reg_no')->on('registration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stu_result_summary');
    }
}
