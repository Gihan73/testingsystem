<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixvalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixval', function (Blueprint $table) {
            $table->string('fixval_id',5);
            $table->string('fixval_des',50);
            $table->string('fixval_type',20);
            $table->integer('order');
            $table->char('active',1);
            $table->primary(['fixval_id', 'fixval_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixval');
    }
}
