<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemporyEnrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tempory_enrolls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('reg_no', 15);
            $table->integer('course_year');
            $table->integer('course_semester');
            $table->string('module_code',15);
            $table->timestamps();
            $table->foreign('reg_no')->references('reg_no')->on('registration');
            $table->foreign('module_code')->references('module_code')->on('module_dtl');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tempory_enrolls');
    }
}
