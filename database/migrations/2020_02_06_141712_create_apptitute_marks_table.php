<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApptituteMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apptitute_mark', function (Blueprint $table) {
            $table->bigIncrements('apptitute_row_no');
            $table->string('reg_no', 15)->unique();
            $table->string('department',50);
            $table->integer('current_year');
            $table->string('module_code',10);
            $table->integer('mark');
            $table->integer('rank');
            $table->timestamps();
            $table->foreign('reg_no')->references('reg_no')->on('registration');
            $table->foreign('module_code')->references('module_code')->on('module_dtl');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apptitute_mark');
    }
}
