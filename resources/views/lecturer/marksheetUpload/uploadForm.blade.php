@extends('dashboard.lec_dashboard')

@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>


<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Create Modules To Enroll Form</h3>
    </div>

    <form class="form-horizontal" method="POST" action="{{ Route('store_mark_sheet') }}" enctype="multipart/form-data">
        @csrf

        <div class="card-body">

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="department" class="col-sm-4 col-form-label">Department *</label>
                    <div class="col-sm-12">
                        <select class="form-control select2 department" name="department" id="department" style="width: 100%;">
                            <option selected="selected" value="" >Select Option</option>
                            @foreach($form_dtls['department'] as $department)

                            @if (old('department') == $department['department_code'])
                            <option value="{{ $department['department_code'] }}" selected>{{ $department['department_name'] }}</option>
                            @else
                            <option value="{{ $department['department_code'] }}"> {{ $department['department_name'] }} </option>
                            @endif


                            @endforeach
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('department') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="course" class="col-sm-4 col-form-label">Course *</label>
                    <div class="col-sm-12">
                        <select class="form-control select2" id="course" name="course" style="width: 100%;">
                            <option value="" >choose your option</option>
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('course') }} </span>
                </div>

            </div>

            <div class="form-group row"> 
                <div class="col-sm-6">
                    <label for="module_name" class="col-sm-4 col-form-label">Module *</label>
                    <div class="col-sm-12">
                        <select class="form-control select2" id="module_name" name="module_name" style="width: 100%;">
                            <option value="" >choose your option</option>
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('module_name') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="semester_yr" class="col-sm-4 col-form-label">Semester Start Year</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="semester_yr" name="semester_yr" value="{{ old('semester_yr') }}" placeholder="Semester Start Year">
                    </div>
                    <span class="error"> {{ $errors->first('semester_yr') }} </span>
                </div>

            </div>

            <div class="form-group row">
                
            <!--<div class="col-sm-6">
                    <label for="inputFile" class="col-sm-4 col-form-label">File input *</label>
                    <div class="col-sm-12">
                        <input type="file"  id="inputFile" name="inputFile">
                        <label class="custom-file-label" for="inputFile">Choose file</label>
                    </div>

                    <span class="error"> {{ $errors->first('inputFile') }} </span>
                </div> -->

              <div class="col-sm-6">
                    <label for="inputFile" class="col-sm-4 col-form-label">File input *</label>
                    <div class="col-sm-12">
                    <input type="file"  name="inputFile" id="inputFile" value="{{ old('inputFile') }}">
                    </div>
                    <span class="error"> {{ $errors->first('inputFile') }} </span>
              </div>

            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

        </div>
    </form>
</div>




@endsection


@push('scripts')

<script type="text/javascript"> 

$(document).ready(function () {

    if ($('#department').val()) {
            var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $('#department').val();
            loadDropDownData(ajax_url_faculty);
    }

    $("#department").change(function (event) {
        var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $('#department').val();
        var course =  $('#course').val();
        loadDropDownData(ajax_url_faculty);
    });


    if ($('#course').val()) {

            var department =  $('#department').val();

            var course =  $('#course').val();

            var ajax_url_course = "{{ url('modules_for_course') }}/" + department+'/'+course ;

            loadModuleLlist(ajax_url_course);
    }


    $("#course").change(function (event) {

        var department =  $('#department').val();

        var course =  $('#course').val();
        
        var ajax_url_course = "{{ url('modules_for_course') }}/" + department+'/'+course ;

        loadModuleLlist(ajax_url_course);

    });

});


function loadDropDownData(ajax_url_faculty) {

            $.ajax({

                url: ajax_url_faculty,

                method: 'GET',

                success: function (data) {

                    $('#course').empty();

                    if (data == 'false') {

                        $('#course').append($("<option> choose your option </option>"));

                    } else {

                        $.each(data, function (key, value) {

                            selected = '';

                            if (key == "{{ old('course')}}") {

                                selected = 'selected';
                            }

                            $('#course').append($("<option " + selected + "></option>").attr("value", key).text(value)).trigger('change.select2');
                        });


                        var department =  $('#department').val();

                        var course     =  $('#course option:selected').val();
                        
                        var ajax_url_course = "{{ url('modules_for_course') }}/" + department+'/'+course ;

                        loadModuleLlist(ajax_url_course);

                    }
                }
            });

        }

    function loadModuleLlist(ajax_url_course){

        $.ajax({ 

            url: ajax_url_course,

            method: 'GET',

            success: function (data){

                $('#module_name').empty();

                if (data == 'false') {

                        $('#module_name').append($("<option> choose your option </option>"));

                    } else {

                        $.each(data, function (key, value) {

                            selected = '';

                            if (key == "{{ old('module_name')}}") {

                                selected = 'selected';
                            }

                            $('#module_name').append($("<option " + selected + "></option>").attr("value", key).text(value)).trigger('change.select2');

                        });
                    }

            }

        });

    }

</script>

@endpush