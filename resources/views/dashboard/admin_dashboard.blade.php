@extends('layouts.app')

@section('sidenav')

<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item">
            <a href="{{ Route('home') }}" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                    Users
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ Route('stu_user_data') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Student Registration</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ Route('LEC_user_data') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Lecturer Registration</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ Route('ED_user_data') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>ED Registration</p>
                    </a>
                </li>
            </ul>
        </li>
        <!-- <li class="nav-header">EXAMPLES</li> -->
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Reports
                </p>
            </a>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Attitude test marks
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ route('attitudeTestView') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p> Upload Marks</p>
                    </a>
                </li>
                
                <!-- <li class="nav-header">EXAMPLES</li> -->

            </ul>
        </li>
    </ul>
</nav>


@endsection