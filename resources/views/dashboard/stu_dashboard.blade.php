@extends('layouts.app')

@section('sidenav')

<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
            <a href="{{ Route('home') }}" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ Route('profile') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <!-- <li class="nav-header">EXAMPLES</li> -->

            </ul>
        </li>

        <li class="nav-item">
            <a href="{{ Route('enroll_my_subject') }}" class="nav-link">
                <i class="nav-icon fas fa-graduation-cap"></i>
                <p>
                    Enroll Subjects
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ Route('repeat_page') }}" class="nav-link">
                <i class="nav-icon fas fa-share"></i>
                <p>
                    Repeat Apply
                </p>
            </a>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Request Reports
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Subjects</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Repeat Subjects</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Marks</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Result Sheets</p>
                    </a>
                </li>
                <!-- <li class="nav-header">EXAMPLES</li> -->

            </ul>
        </li>


    </ul>
</nav>


@endsection