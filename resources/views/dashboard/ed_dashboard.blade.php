@extends('layouts.app')

@section('sidenav')

<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item has-treeview">
            <a href="{{ Route('home') }}" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ Route('profile') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>User Profile</p>
                    </a>
                </li>
                <!-- <li class="nav-header">EXAMPLES</li> -->

            </ul>
        </li>

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-server"></i>
                <p>
                    Modules
                    <i class="right fas fa-angle-left"></i>
                </p>
            </a>
            <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="{{ Route('module_result') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Create Modules</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{ Route('module_to_enroll_result') }}" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Create Modules To Enroll</p>
                    </a>
                </li>
                <!-- <li class="nav-header">EXAMPLES</li> -->

            </ul>
        </li>

        <!--<li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-graduation-cap"></i>
                <p>
                    Enroll Subjects
                </p>
            </a>
        </li>-->

        <li class="nav-item">
            <a href="{{ Route('pnding_mrksht_list') }}" class="nav-link">
                <i class="nav-icon fas fa-upload"></i>
                <p>
                    Approve Marks
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ Route('pnding_repeat_list') }}" class="nav-link">
                <i class="nav-icon fas fa-share"></i>
                <p>

                    Repeat Approved
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon fas fa-folder-open"></i>
                <p>
                    Apptitude Test Marks
                </p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ url('action_pool') }}" class="nav-link">
                <!-- <ion-icon name="layers-outline"></ion-icon> -->
                <i class="nav-icon fab fa-buffer"></i>
                <p>
                    Action Pool
                </p>
            </a>
        </li>

    </ul>
</nav>


@endsection