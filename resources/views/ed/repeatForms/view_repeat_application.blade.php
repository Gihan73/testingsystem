@extends('dashboard.ed_dashboard')

@section('content')

<div class="card">

    <div class="card-header">
        <h3 class="card-title">Repeat Subject List</h3>
    </div>
    
    <div class="card-body">

        <table id="stud_reg_result" class="table table-bordered table-striped">
            
            <thead>
                <tr>
                    <th>Module ID</th>
                    <th>Module Name</th>
                    <th>No of Credit</th>
                    <th>Repeat Price</th>
                </tr>
            </thead>

            <tbody>
                 @foreach($appDtl['repeat_module'] as $result )
                    <tr>
                       
                        <td>{{ $result['module_code'] }}</td>
                        <td>{{ $result['module_name'] }}</td>
                        <td>{{ $result['no_of_credit'] }}</td>
                        <td>{{ $result['repeat_price'] }}</td>
                   
                    </tr>

                @endforeach

            </tbody>
            
        </table>
    </div>
  
</div>

<div class="card card-info">
	

    <div class="card-header">
        <h3 class="card-title"> Repeat Subject Form</h3>
    </div>
   

        <div class="card-body">   

    <form class="form-horizontal" method="POST" action="{{ Route('approve_rpt_frm',['app_id' => $appDtl['application_id'] ]) }}" > 
        @csrf
           
			<div class="form-group row">

                <div class="col-sm-6">

	                    <label for="stu_name" class="col-sm-4 col-form-label">Student Name</label>
	                    <div class="col-sm-10">
	                        <input type="text" class="form-control" id="stu_name" name="stu_name" value="{{ $appDtl['student']['fname'].' '.$appDtl['student']['lname'] }}" readonly>
	                    </div>

                </div>
                <div class="col-sm-6">

                        <label for="apply_date" class="col-sm-4 col-form-label">Apply On</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="apply_date" name="apply_date" value="{{ $appDtl['apply_date'] }}" readonly>
                        </div>

                </div>

            </div>
            <div class="form-group row">

                <div class="col-sm-6">

                        <label for="department" class="col-sm-4 col-form-label">Department</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="department" name="department" value="{{ $appDtl['student']['department_name'] }}" readonly>
                        </div>

                </div>

                <div class="col-sm-6">

                        <label for="course" class="col-sm-4 col-form-label">Course</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="course" name="course" value="{{ $appDtl['student']['course'] }}" readonly>
                        </div>

                </div>

            </div>
            <div class="form-group row">

                <div class="col-sm-6">

                        <label for="repeat_fee" class="col-sm-4 col-form-label">Repeat Fee</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="repeat_fee" name="repeat_fee" value="{{ $appDtl['repeat_fee'] }}" readonly>
                        </div>

                </div>

                <div class="col-sm-6">

                        <label for="payment_receipt" class="col-sm-4 col-form-label">Payment Document</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="payment_receipt" name="payment_receipt" value="{{ $appDtl['receipt_img'] }}" readonly>
                        </div>

                </div>

            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-info" >Approve</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

    </form>
        </div>
</div>

@endsection

@push('scripts')

<script type="text/javascript">

	$(document).ready(function () {

		

	});

</script>

@endpush