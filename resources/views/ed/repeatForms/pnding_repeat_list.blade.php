@extends('dashboard.ed_dashboard')

@section('content')


<div class="card">

    <div class="card-header">

        <h3 class="card-title">Pending Repeat Forms For Approving</h3>

    </div>

    <div class="card-body">

        <table id="stud_reg_result" class="table table-bordered table-striped">

            <thead>

                <tr>

                    <th>Application ID</th>

                    <th>Student Name</th>

                    <th>Department</th>

                    <th>Course</th>

                    <th>Evaluate</th>

                </tr>

            </thead>

            <tbody>



                @foreach($form_dtls as $data)

                <tr>

                    <td>{{ $data['application_id'] }}</td>

                    <td>{{ $data['student']['fname'].' '.$data['student']['lname'] }}</td>

                    <td>{{ $data['student']['department_name'] }}</td>

                    <td>{{ $data['student']['course_name'] }}</td>

                    <td>
                        <a type="button" class="btn btn-info" href="{{ 
                        Route('rpt_application_vw',['app_id' => $data['application_id'] ])}}" >Approve</a>
                    </td>

                </tr>

                @endforeach

            </tbody>

            <tfoot>

                <tr>

                    <th>Application ID</th>

                    <th>Student Name</th>

                    <th>Department</th>

                    <th>Course</th>

                    <th>Evaluate</th>


                </tr>

            </tfoot>

        </table>

    </div>

</div>

@endsection

@push('scripts')

<script type="text/javascript">

    $(document).ready(function () {        
    
        // $("#aprv_mrksht").click(function (event) {

        //     var file_name = $(this).attr("data-file");

        //     var semester_yr = $(this).attr("data-yr");

        //     read_marksheet(file_name, semester_yr);

        // });
    });

    // function read_marksheet(file_name, semester_yr){

    //     console.log(file_name);

    //     $.ajax({

    //         url : "{{ url('read_n_approve') }}/" + file_name + "/" +semester_yr,

    //         method : 'GET',

    //         success: function (data){
                
    //             location.reload(true);

    //         }

    //     });
    // }

</script>

@endpush