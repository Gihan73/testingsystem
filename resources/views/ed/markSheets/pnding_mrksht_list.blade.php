@extends('dashboard.ed_dashboard')

@section('content')


<div class="card">

    <div class="card-header">

        <h3 class="card-title">Pending Mark Sheets For Approving</h3>

    </div>

    <div class="card-body">

        <table id="stud_reg_result" class="table table-bordered table-striped">

            <thead>

                <tr>

                    <th>File Name</th>

                    <th>Uploaded Lecturer</th>

                    <th>Semester Started Year</th>

                    <th>Evaluate</th>

                </tr>

            </thead>

            <tbody>

                @foreach($form_dtls as $data)

                @php 
                    $file_name = explode("/", $data['document_path'])[1];
                @endphp
                <tr>

                    <td>{{ $file_name }}</td>

                    <td>{{ $data['semester_yr'] }}</td>

                    <td>{{ $data['created_by'] }}</td>

                    <td><input type="button" id="aprv_mrksht" data-file="{{ $file_name }}" data-yr="{{ $data['semester_yr'] }}" class="btn btn-info" value="Approved"></td>

                </tr>

                @endforeach

            </tbody>

            <tfoot>

                <tr>

                    <th>File Name</th>

                    <th>Uploaded Lecturer</th>

                    <th>Evaluate</th>

                </tr>

            </tfoot>

        </table>

    </div>

</div>

@endsection

@push('scripts')

<script type="text/javascript">

    $(document).ready(function () {        
    
        $("#aprv_mrksht").click(function (event) {

            var file_name = $(this).attr("data-file");

            var semester_yr = $(this).attr("data-yr");

            read_marksheet(file_name, semester_yr);

        });
    });

    function read_marksheet(file_name, semester_yr){

        console.log(file_name);

        $.ajax({

            url : "{{ url('read_n_approve') }}/" + file_name + "/" +semester_yr,

            method : 'GET',

            success: function (data){
                
                location.reload(true);

                //reload page

                // $('#result_content').html(data);

            }

        });
    }

</script>

@endpush