@extends('dashboard.ed_dashboard')

@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>


<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title"> Module Create Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->

    <form class="form-horizontal" method="POST" action="{{ Route('module_store') }}">
        @csrf

        <div class="card-body">

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="module_name" class="col-sm-4 col-form-label">Module Name * </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="module_name" name="module_name" value="{{ old('module_name') }}" placeholder="Module Name">
                    </div>
                    <span class="error"> {{ $errors->first('module_name') }} </span>
                </div>


                <div class="col-sm-6">
                    <label for="noOfCredits" class="col-sm-4 col-form-label">No Of Credits * </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="noOfCredits" name="noOfCredits" value="{{ old('noOfCredits') }}" placeholder="No Of Credits">
                    </div>
                    <span class="error"> {{ $errors->first('noOfCredits') }} </span>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="repeat_cost" class="col-sm-4 col-form-label">Repeat Cost * </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="repeat_cost" name="repeat_cost" value="{{ old('repeat_cost') }}" placeholder="Repeat Cost">
                    </div>
                    <span class="error"> {{ $errors->first('repeat_cost') }} </span>
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

        </div>
    </form>
</div>




@endsection
