@extends('dashboard.ed_dashboard')

@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>


<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Create Modules To Enroll Form</h3>
    </div>

    <form class="form-horizontal" method="POST" action="{{ Route('module_enroll_store') }}">
        @csrf

        <div class="card-body">

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="module_name" class="col-sm-4 col-form-label">Module Name *</label>

                    <div class="col-sm-12">
                        <select class="form-control select2 module_name" name="module_name" id="module_name" style="width: 100%;">
                            <option selected="selected" value="" >Select Option</option>
                            @foreach($form_dtls['modules'] as $module)



                            @if (old('module_name') == $module['module_code'])
                            <option value="{{ $module['module_code'] }}" selected>{{ $module['module_name'] }}</option>
                            @else
                            <option value="{{ $module['module_code'] }}"> {{ $module['module_name'] }} </option>
                            @endif


                            @endforeach
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('module_name') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="department" class="col-sm-4 col-form-label">Department *</label>

                    <div class="col-sm-12">
                        <select class="form-control select2 department" name="department" id="department" style="width: 100%;">
                            <option selected="selected" value="" >Select Option</option>
                            @foreach($form_dtls['faculties'] as $faculty)



                            @if (old('department') == $faculty['department_code'])
                            <option value="{{ $faculty['department_code'] }}" selected>{{ $faculty['department_name'] }}</option>
                            @else
                            <option value="{{ $faculty['department_code'] }}"> {{ $faculty['department_name'] }} </option>
                            @endif


                            @endforeach
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('department') }} </span>
                </div>
            </div>

            <div class="form-group row"> 

                <div class="col-sm-6">
                    <label for="course" class="col-sm-4 col-form-label">Course *</label>
                    <div class="col-sm-12">
                        <select class="form-control select2" id="course" name="course" style="width: 100%;">
                            <option value="" >choose your option</option>
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('course') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="course_year" class="col-sm-4 col-form-label">Course Year </label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="course_year" name="course_year" value="{{ old('course_year') }}" placeholder="Course Year">
                    </div>
                    <span class="error"> {{ $errors->first('course_year') }} </span>
                </div>

            </div>

            <div class="form-group row"> 

                <div class="col-sm-6">
                    <label for="semester" class="col-sm-4 col-form-label">Semester </label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="semester" name="semester" value="{{ old('semester') }}" placeholder="Semester">
                    </div>
                    <span class="error"> {{ $errors->first('semester') }} </span>
                </div>


                <div class="col-sm-6">
                    <label for="lecturer" class="col-sm-4 col-form-label">Lecturer *</label>

                    <div class="col-sm-12">
                        <select class="form-control select2 lecturer" name="lecturer" id="lecturer" style="width: 100%;">
                            <option selected="selected" value="" >Select Option</option>

                            @foreach($form_dtls['lecturers'] as $lecturer)




                            @if (old('lecturer') == $lecturer['reg_no'])
                            <option value="{{ $lecturer['reg_no'] }}" selected>
                                {{ $lecturer['fname'].' '.$lecturer['lname'] }}
                            </option>
                            @else
                            <option value="{{ $lecturer['reg_no'] }}"> {{ $lecturer['fname'].' '.$lecturer['lname'] }} </option>
                            @endif


                            @endforeach
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('lecturer') }} </span>
                </div>

            </div>


            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="assign_markr" class="col-sm-10 col-form-label">Continues Assignment Mark Ratio </label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="assign_markr" name="assign_markr" value="{{ old('assign_markr') }}" placeholder="Continues Assignment Mark">
                    </div>
                    <span class="error"> {{ $errors->first('assign_markr') }} </span>
                </div>


                <!-- <div class="col-sm-6">
                    <label for="exam_markr" class="col-sm-10 col-form-label">Exam Mark Ratio </label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="exam_markr" name="exam_markr" value="{{ old('exam_markr') }}" placeholder="Exam Mark Ratio">
                    </div>
                <span class="error"> {{ $errors->first('exam_markr') }} </span>
                </div> -->

            </div>



            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

        </div>
    </form>
</div>




@endsection

@push('scripts')

<script type="text/javascript">

    $(document).ready(function () {

        var course = "{{ old('course') }}";

        //when page load is there selected facalty , load depending data
        if ($('#department').val()) {

            var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $('#department').val();

            loadDropDownData(ajax_url_faculty);



        }

        //value will change after select faculty
        $("#department").change(function (event) {

            var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $(this).val();

            loadDropDownData(ajax_url_faculty);

        });

    });

    function loadDropDownData(ajax_url_faculty) {

        $.ajax({

            url: ajax_url_faculty,

            method: 'GET',

            success: function (data) {

                $('#course').empty();

                if (data == 'false') {

                    $('#course').append($("<option> choose your option </option>"));

                } else {


                    $.each(data, function (key, value) {

                        selected = '';

                        if (key == course) {

                            selected = 'selected';
                        }

                        $('#course').append($("<option " + selected + "></option>").attr("value", key).text(value)).trigger('change.select2');


                    });
                }
            }
        });

    }


</script>

@endpush