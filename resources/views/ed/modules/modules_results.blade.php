@extends('dashboard.ed_dashboard')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Modules List</h3>
        <a type="button" href="{{ Route('module_create') }}" class="btn btn-block btn-primary col-sm-2 float-right">Create</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <table id="stud_reg_result" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Module Code</th>
                    <th>Module Name</th>
                    <th>No Of Credits</th>
                    <th>Repeat Cost</th>
                    <!-- <th>Action</th> -->
                </tr>
            </thead>
            <tbody>
                @foreach($getAll as $data)
                <tr>

                    <td>{{ $data['module_code'] }}</td>
                    <td>{{ $data['module_name'] }}</td>
                    <td>{{ $data['no_of_credit'] }}</td>
                    <td>{{ $data['repeat_price'] }}</td>
                    <!-- <td></td> -->
                </tr>
                @endforeach


            </tbody>
            <tfoot>
                <tr>
                    <th>Module Code</th>
                    <th>Module Name</th>
                    <th>No Of Credits</th>
                    <th>Repeat Cost</th>
                    <!-- <th>Action</th> -->
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>




@endsection