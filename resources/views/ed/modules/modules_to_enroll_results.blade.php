@extends('dashboard.ed_dashboard')

@section('content')

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Enroll Module List</h3>
        <a type="button" href="{{ Route('module_create_to_enroll') }}" class="btn btn-block btn-primary col-sm-2 float-right">Create</a>
    </div>

    <div class="card-body">

        <table id="stud_reg_result" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Module Name</th>
                    <th>Department</th>
                    <th>Course</th>
                    <th>Semester</th>
                    <th>Lecturer</th>
                    <!-- <th>Action</th> -->
                </tr>
            </thead>
            <tbody>

                @foreach($getAll as $data)

                @foreach($data as $row_data)

                <tr>

                    <td>{{ $row_data['module_dtl']['module_name'] }}</td>
                    <td>{{ $row_data['department']['department_name'] }}</td>
                    <td>{{ $row_data['course']['course_name'] }}</td>
                    <td>{{ $row_data['semester'] }}</td>
                    <td>{{ $row_data['registration']['fname'].' '.$row_data['registration']['lname'] }}</td>
                    <!-- <td></td> -->
                </tr>
                @endforeach

                @endforeach


            </tbody>
            <tfoot>
                <tr>
                    <th>Module Code</th>
                    <th>Department</th>
                    <th>Course</th>
                    <th>Semester</th>
                    <th>Lecturer</th>
                    <!-- <th>Action</th> -->
                </tr>
            </tfoot>
        </table>
    </div>
</div>




@endsection