@extends('dashboard.ed_dashboard')


@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>

<div class="card">
    <div class="card-header">
        <h3 class="card-title">Action Pool</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">

        <table id="stud_reg_result" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Description</th>
                    <th></th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Generating Final MArks and Grades</td>
                    <td><input type="text" name="year" id="year_to_final_mark" value=""></td>
                    <td><input type="button" id="generate_final_mark" class="btn btn-info" value="Final MArk & Grade"></td>
                </tr>
                <tr>
                    <td>Generating GPA Semester Vice</td>
                    <td><!-- <input type="text" name="year" id="year_to_final_mark" value=""> --></td>
                    <td><input type="button" id="semster_end_gpa" class="btn btn-info" value="Semester GPA"></td>
                </tr>
                <tr>
                    <td>Generating Overall GPA Of Course</td>
                    <td><!-- <input type="text" name="year" id="year_to_final_mark" value=""> --></td>
                    <td><input type="button" id="overall_gpa" class="btn btn-info" value="Overall GPA"></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>Description</th>
                    <th></th>
                    <th>Module Name</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>

@endsection


@push('scripts')

<script type="text/javascript">

	$(document).ready(function () {

		$("#generate_final_mark").click(function (event) {

            var year = $('#year_to_final_mark').val();

            if(year == ''){

                $('#year_to_final_mark').closest('td').append($("<span class='error'>  required field</span>"));
                
            }else{

                $('span').empty();

                generateFinalMark(year);
            }
        });


        $("#semster_end_gpa").click(function (event) {

	        	generateSemesterGPA();
			
	    });


        $("#overall_gpa").click(function (event) {

                generateOverallGPA();
            
        });

    });

    function generateFinalMark(year){

    	$.ajax({

                url: "{{ url('generate_final_mark') }}/" + year ,

                method: 'GET',

                success: function (data) {

                	$('#year_to_final_mark').val('');

                	if(data == 'true'){

                		Swal.fire({ icon: 'success', title: 'Success', text: "Marks graded successfully" });

                	}else{

                		Swal.fire({ icon: 'error', title: 'Error', text: "Marks graded has error" });

                	}
                    
                }
            });
    }


    function generateSemesterGPA(){

        $.ajax({

            url: "{{ url('semester_gpa_calculate') }}",

            method : "GET",

            success: function (data) {

                if(data == 'true'){

                    Swal.fire({ icon: 'success', title: 'Success', text: "Marks graded successfully" });

                }else{

                    Swal.fire({ icon: 'error', title: 'Error', text: "Marks graded has error" });

                }
            }

        });

    }


    function generateOverallGPA(){

        $.ajax({

            url: "{{ url('overall_gpa_calculate') }}",

            method : "GET",

            success: function (data) {

                if(data == 'true'){

                    Swal.fire({ icon:'success', title:'Success', text:"Calculate Overall GPA Process Complete"});

                }else{
                    
                    Swal.fire({ icon:'error', title:'Error', text:"Calculate Overall GPA Process Has Error"});
                }
            }
        });
    }

</script>

@endpush