@extends( $data['extend_blade'] )

@section('content')



<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>


<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title"> {{ $data['label'] }} Profile</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->

    <form class="form-horizontal" method="POST" action="{{-- $data['store_url'] --}}">
        @csrf

        <div class="card-body">

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="fname" class="col-sm-4 col-form-label">First Name  : </label>
                    Hasini
                </div>


                <div class="col-sm-6">
                    <label for="lname" class="col-sm-4 col-form-label">Last Name : </label>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="nic" class="col-sm-4 col-form-label">National ID No : </label>
                </div>

                <div class="col-sm-6">
                    <label for="email" class="col-sm-4 col-form-label">Email : </label>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-12">
                    <label for="address" class="col-sm-4 col-form-label">Address : </label>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="mobile" class="col-sm-4 col-form-label">Mobile : </label>
                </div>

                <div class="col-sm-6">
                    <label for="username" class="col-sm-4 col-form-label">Username : </label>
                </div>

            </div>


        </div>
    </form>
</div>




@endsection

@push('scripts')

<script type="text/javascript">


</script>

@endpush