@extends('dashboard.stu_dashboard')

@section('content')

<div id="result_content">


</div>

@endsection

@push('scripts')

<script type="text/javascript">

    $(document).ready(function () {

        load_page();

        $('#result_content').on('click', '#enroll_sub', function(event) {

            var enroll_id = $(this).attr("data-module");

            $.ajax({

              url : "{{ url('enroll_subject') }}/" + enroll_id,

              method : 'GET',

              success: function (data){

                $('#result_content').html(data);

              }

            });

        });

        $('#result_content').on('click', '#unenroll_sub', function(event) {

            var unenroll_id = $(this).attr("data-submodule");

            $.ajax({

              url : "{{ url('unenroll_subject') }}/" + unenroll_id,

              method : 'GET',

              success: function (data){

                $('#result_content').html(data);

              }

            });

        });


        $('#result_content').on('click', '#submit_subs', function(event) {

          var ajax_url =  "{{ url('submit_subjects') }}";

          Swal.fire({
            title: 'Are you sure?',
            text: "Once you submit you can't change subjects any reason.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#28a745',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Confirm'
          }).then((result) => {
            if (result.value) {
              $.get(ajax_url, function(data){
                  Swal.fire({
                      icon: 'success',
                      title: 'Submitted',
                      text: "Your enrollment success"
                  });

                });
            }
          });

    });

});
    function load_page() {

        $.ajax({

            url: "{{ url('getall_subject') }}",

            method: 'GET',

            success: function (data) {

                $('#result_content').html(data);
            }
        });
    }

</script>

@endpush