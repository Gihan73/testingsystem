<div class="card">
    <div class="card-header border-transparent">
        <h3 class="card-title">Enroll Your Subjects</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>

        </div>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered table-striped datatable" id="pending">
                <thead>
                    <tr>
                        <th>Module ID</th>
                        <th>Enroll ID</th>
                        <th>Module Name</th>
                        <th>No of Credit</th>
                        <th>Lecturer</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($result['pending_results'] as $data )
                    <tr>
                        <td>{{ $data['module_code'] }}</td>
                        <td>{{ $data['enroll_id'] }}</td>
                        <td>{{ $data['module_dtl']['module_name'] }}</td>
                        <td>{{ $data['module_dtl']['no_of_credit'] }}</td>
                        <td>{{ $data['registration']['fname'].' '.$data['registration']['lname'] }}</td>
                        <td><button type="button" class="btn btn-sm btn-info" id="enroll_sub" data-module="{{ $data['enroll_id'] }}" >Enroll</button></td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="card" id="subject_form">
    <div class="card-header border-transparent">
        <h3 class="card-title">Enrolled Subjects</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>

        </div>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th>Module ID</th>
                        <th>Enroll ID</th>
                        <th>Module Name</th>
                        <th>No of Credit</th>
                        <th>Lecturer</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($result['tempory_results'] as $result )
                    <tr>
                        <td>{{ $result['module_code'] }}</td>
                        <td>{{ $result['enroll_id'] }}</td>
                        <td>{{ $result['module_dtl']['module_name'] }}</td>
                        <td>{{ $result['module_dtl']['no_of_credit'] }}</td>
                        <td>{{ $result['registration']['fname'].' '.$result['registration']['lname'] }}</td>
                        <td><button type="button" class="btn btn-sm btn-secondary" id="unenroll_sub" data-submodule="{{ $result['enroll_id'] }}" >Unenroll</button></td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
    <div class="card-footer clearfix">
        <button type="button" class="btn btn-sm btn-info float-right" id="submit_subs" >Submit</button>
    </div>
</div>



