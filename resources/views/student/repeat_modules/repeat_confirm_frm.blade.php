@extends('dashboard.stu_dashboard')

@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>


<div class="card">

    <div class="card-header">
        <h3 class="card-title">Applying Repeat List</h3>
    </div>
    
    <div class="card-body">

        <table id="stud_reg_result" class="table table-bordered table-striped">
            
            <thead>
                <tr>
                    <th>Module ID</th>
                    <th style="display:none;">Enroll ID</th>
                    <th>Module Name</th>
                    <th>No of Credit</th>
                </tr>
            </thead>

            <tbody>
                 @foreach($results['tempory_results'] as $result )
                    <tr>
                       
                        <td>{{ $result['module_dtl']['module_code'] }}</td>
                        <td style="display:none;">{{ $result['enroll_id'] }}</td>
                        <td>{{ $result['module_dtl']['module_name'] }}</td>
                        <td>{{ $result['module_dtl']['no_of_credit'] }}</td>
                   
                    </tr>

                @endforeach

            </tbody>
            
        </table>
    </div>
  
</div>

<div class="card card-info">
	

    <div class="card-header">
        <h3 class="card-title"> Repeat Subject Confirm Form</h3>
    </div>
   

        <div class="card-body">   



            <!-- <div class="form-group row"></div>
            <div class="form-group row"></div>
            <div class="form-group row"></div> -->

    <form class="form-horizontal" method="POST" action="{{ route('submit_repeat') }}" enctype="multipart/form-data"> 
        @csrf

           
			<div class="form-group row">

                <div class="col-sm-6">

                	<div class="form-group row">

	                    <label for="rptfee" class="col-sm-4 col-form-label">Repeat Fee * </label>
	                    <div class="col-sm-10">
	                        <input type="text" class="form-control" id="rptfee" name="rptfee" value="{{ $results['sum'] }}" placeholder="Repeat Cost" readonly>
	                    </div>
	                    <span class="error"> {{ $errors->first('rptfee') }} </span>
                

		                <label for="upload_receipt" class="col-sm-4 col-form-label">Upload Receipt </label>
		                <div class="col-sm-10">
		                    <input type="file" id="upload_receipt" name="upload_receipt" value="{{ old('upload_receipt') }}" placeholder="Last Name">
		                </div>
		                <span class="error"> {{ $errors->first('upload_receipt') }} </span>

                	</div>

                </div>

                <div class="col-sm-6">

                	<img id="image_preview_container" src="{{ asset('storage/image/default.jpg') }}" alt="preview image" style="max-height: 240px;max-width: 500px;">

                	<img id="invalid_img" src="{{ asset('storage/image/error_format.png') }}" alt="invalid format" style="max-height: 240px;max-width: 500px;">
            
           		</div>

            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-info" >Submit</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

    </form>
        </div>
</div>

@endsection

@push('scripts')

<script type="text/javascript">

	$(document).ready(function () {

		$('#invalid_img').hide();

		$('#upload_receipt').on('change', function() {
          
            let reader = new FileReader();

            reader.onload = (e) => { 

            	var allowed   = ["jpeg", "png", "jpg", "svg"];

            	var extension = (e.target.result.split(";")[0]).split("/")[1];

            	if(allowed.indexOf(extension) > -1){

              		$('#image_preview_container').attr('src', e.target.result);

              		$('#image_preview_container').show();

              		$('#invalid_img').hide();

            	}else{

            		$('#image_preview_container').hide();

              		$('#invalid_img').show();

            	}
            	
              
            }

            reader.readAsDataURL(this.files[0]); 
 
        });

	});

</script>

@endpush