<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>

<div class="card">
    <div class="card-header border-transparent">
        <h3 class="card-title">Enroll Your Repeat Subjects</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>

        </div>
    </div>
    <div class="card-body p-0">
        <div class="table-responsive">
            <table class="table table-bordered table-striped datatable" id="pending">
                <thead>
                    <tr>
                        <th>Module ID</th>
                        <th style="display:none;">Enroll ID</th>
                        <th>Module Name</th>
                        <th>No of Credit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($results['pending_results'] as $data )
                    <tr>
                        <td>{{ $data['module_dtl']['module_code'] }}</td>
                        <td style="display:none;">{{ $data['enroll_id'] }}</td>
                        <td>{{ $data['module_dtl']['module_name'] }}</td>
                        <td>{{ $data['module_dtl']['no_of_credit'] }}</td>
                        <td><button type="button" class="btn btn-sm btn-info" id="enroll_rp_sub" data-module="{{ $data['enroll_id'] }}" >Apply</button></td>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="card" id="repeat_subject_form">
    <div class="card-header border-transparent">
        <h3 class="card-title">Repeat Apply Subjects</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                <i class="fas fa-minus"></i>
            </button>

        </div>
    </div>
     
    <div class="card-body p-0"> 
        <div class="table-responsive">
            <table class="table table-bordered table-striped datatable">
                <thead>
                    <tr>
                        <th>Module ID</th>
                        <th style="display:none;">Enroll ID</th>
                        <th>Module Name</th>
                        <th>No of Credit</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($results['tempory_results'] as $result )
                    <tr>
                        <tr>
                        <td>{{ $result['module_dtl']['module_code'] }}</td>
                        <td style="display:none;">{{ $result['enroll_id'] }}</td>
                        <td>{{ $result['module_dtl']['module_name'] }}</td>
                        <td>{{ $result['module_dtl']['no_of_credit'] }}</td>
                        <td><button type="button" class="btn btn-sm btn-info" id="not_apply_rp_sub" data-submodule="{{ $result['enroll_id'] }}" >Not Apply</button></td>
                    </tr>
                    </tr>

                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

    <div class="card-footer clearfix">
        <button type="button" class="btn btn-sm btn-info float-right" id="apply_rpt_sub" >Submit</button>
    </div>





















































    <!-- <div class="card-footer clearfix">
    <form class="form-horizontal" id="repeat_apply_frm" method="POST" enctype="multipart/form-data" action="{{ url('submit_repeat') }}" >
        @csrf
        
        <div class="form-group row">
            <div class="col-sm-6">

                <div class="form-group row">

                    <label for="rptfee" class="col-sm-4 col-form-label">Repeat Fee </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="rptfee" name="rptfee" value="{{-- $results['sum'] --}}" placeholder="Repeat Fee" readonly>
                    </div>
                    <span class="error"> {{-- $errors->first('rptfee') --}} </span>


                    <label for="upload_receipt" class="col-sm-4 col-form-label">Upload Receipt </label>
                    <div class="col-sm-10">
                        <input type="file" id="upload_receipt" name="upload_receipt" value="{{ old('upload_receipt') }}" placeholder="Last Name">
                    </div>
                    <span class="error"> {{-- $errors->first('upload_receipt') --}} </span>

                </div>

            </div>

            <div class="col-sm-6"> -->

                <!-- <img id="image_preview_container" src="{{ asset('storage/image/default.jpg') }}" alt="preview image" style="max-height: 240px;"> -->
            
           <!--  </div>

        </div>
        <span class="error">*Once you submit you can't change subjects any reason.</span>
        <button type="submit" class="btn btn-sm btn-info float-right" id="submit_repeat_subs" >Submit</button>
        
    </form>
    </div> -->

<!-- </div> -->
