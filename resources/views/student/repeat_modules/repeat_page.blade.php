@extends('dashboard.stu_dashboard')

@section('content')

<div id="result_content">


</div>

@endsection

@push('scripts')

<script type="text/javascript">
	
	$(document).ready(function () {

		load_page();

        $('#result_content').on('click', '#enroll_rp_sub', function(event) {

            var enroll_id = $(this).attr("data-module");

            $.ajax({

              url : "{{ url('enroll_repeat_subject') }}/" + enroll_id,

              method : 'GET',

              success: function (data){

                $('#result_content').html(data);

              }

            });

        });

        $('#result_content').on('click', '#not_apply_rp_sub', function(event) {

            var unenroll_id = $(this).attr("data-submodule");

            $.ajax({

              url : "{{ url('unenroll_repeat_subject') }}/" + unenroll_id,

              method : 'GET',

              success: function (data){

                $('#result_content').html(data);

              }

            });

        });


        $('#result_content').on('click', '#apply_rpt_sub', function(event) {

          var ajax_url =  "{{ url('confirm_form') }}";

              Swal.fire({
                title: 'Are you sure?',
                text: "Once you submit you can't change subjects any reason.",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#28a745',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Confirm'
              }).then((result) => {
                if (result.value) {
                  $.get(ajax_url, function(data){
                      Swal.fire({
                          icon: 'success',
                          title: 'Submitted',
                          text: "Your enrollment success"
                      }).then((result) =>{
                        if(result.value){
                          window.location.replace('{{ url('confirm_form') }}');
                        } 
                      });

                    });
                }
              });

        });
	});

	function load_page() {

        $.ajax({

            url: "{{ url('repeat_list') }}",

            method: 'GET',

            success: function (data) {

                $('#result_content').html(data);
            }
        });
    }

</script>

@endpush