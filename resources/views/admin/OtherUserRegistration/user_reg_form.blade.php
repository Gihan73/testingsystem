@extends('dashboard.admin_dashboard')

@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>


<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title"> {{ $data['label'] }} Registration Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->

    <form class="form-horizontal" method="POST" action="{{ $data['store_url'] }}">
        @csrf

        <div class="card-body">

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="fname" class="col-sm-4 col-form-label">First Name * </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="fname" name="fname" value="{{ old('fname') }}" placeholder="First Name">
                    </div>
                    <span class="error"> {{ $errors->first('fname') }} </span>
                </div>


                <div class="col-sm-6">
                    <label for="lname" class="col-sm-4 col-form-label">Last Name * </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="lname" name="lname" value="{{ old('lname') }}" placeholder="Last Name">
                    </div>
                    <span class="error"> {{ $errors->first('lname') }} </span>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="nic" class="col-sm-4 col-form-label">National ID No * </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nic" name="nic" value="{{ old('nic') }}" placeholder="National ID No">
                    </div>
                    <span class="error"> {{ $errors->first('nic') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="email" class="col-sm-4 col-form-label">Email * </label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email">
                    </div>
                    <span class="error"> {{ $errors->first('email') }} </span>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-12">
                    <label for="address" class="col-sm-4 col-form-label">Address * </label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="Address">
                    </div>
                    <span class="error"> {{ $errors->first('address') }} </span>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="mobile" class="col-sm-4 col-form-label">Mobile * </label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile">
                    </div>
                    <span class="error"> {{ $errors->first('mobile') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="username" class="col-sm-4 col-form-label">Username * </label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" placeholder="Username">
                    </div>
                    <span class="error"> {{ $errors->first('username') }} </span>
                </div>

            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="password" class="col-sm-4 col-form-label">Password *</label>
                    <div class="col-sm-12">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <span class="error"> {{ $errors->first('password') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="password_confirm" class="col-sm-4 col-form-label">Password Confirm *</label>
                    <div class="col-sm-12">
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Your Password ">
                    </div>
                    <span class="error"> {{ $errors->first('password_confirm') }} </span>
                </div>

            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

        </div>
    </form>
</div>




@endsection

@push('scripts')

<script type="text/javascript">


</script>

@endpush