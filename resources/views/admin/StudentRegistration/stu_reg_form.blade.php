@extends('dashboard.admin_dashboard')

@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>

<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Student Registration Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form class="form-horizontal" method="POST" action="{{ route('store_stu_reg') }}">
        @csrf

        <div class="card-body">

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="fname" class="col-sm-4 col-form-label">First Name *</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="fname" name="fname" value="{{ old('fname') }}" placeholder="First Name">
                    </div>
                    <span class="error"> {{ $errors->first('fname') }} </span>
                </div>


                <div class="col-sm-6">
                    <label for="lname" class="col-sm-4 col-form-label">Last Name *</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="lname" name="lname" value="{{ old('lname') }}" placeholder="Last Name">
                    </div>
                    <span class="error"> {{ $errors->first('lname') }} </span>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="nic" class="col-sm-4 col-form-label">National ID No *</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="nic" name="nic" value="{{ old('nic') }}" placeholder="National ID No">
                    </div>
                    <span class="error"> {{ $errors->first('nic') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="email" class="col-sm-4 col-form-label">Email *</label>
                    <div class="col-sm-12">
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}" placeholder="Email">
                    </div>
                    <span class="error"> {{ $errors->first('email') }} </span>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-12">
                    <label for="address" class="col-sm-4 col-form-label">Address *</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="Address">
                    </div>
                    <span class="error"> {{ $errors->first('address') }} </span>
                </div>
            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="mobile" class="col-sm-4 col-form-label">Mobile *</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="mobile" name="mobile" value="{{ old('mobile') }}" placeholder="Mobile">
                    </div>
                    <span class="error"> {{ $errors->first('mobile') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="username" class="col-sm-4 col-form-label">Username *</label>
                    <div class="col-sm-12">
                        <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}" placeholder="Username">
                    </div>
                    <span class="error"> {{ $errors->first('username') }} </span>
                </div>

            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="password" class="col-sm-4 col-form-label">Password *</label>
                    <div class="col-sm-12">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <span class="error"> {{ $errors->first('password') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="password_confirm" class="col-sm-4 col-form-label">Password Confirm *</label>
                    <div class="col-sm-12">
                        <input type="password" class="form-control" id="password_confirm" name="password_confirm" placeholder="Confirm Your Password ">
                    </div>
                    <span class="error"> {{ $errors->first('password_confirm') }} </span>
                </div>

            </div>

            <div class="form-group row">

                <div class="col-sm-6">
                    <label for="department" class="col-sm-4 col-form-label">Department *</label>

                    <div class="col-sm-12">
                        <select class="form-control select2 department" name="department" id="department" style="width: 100%;">
                            <option selected="selected" value="" >Select Option</option>
                            @foreach($form_dtls['faculties'] as $faculty)



                            @if (old('department') == $faculty['department_code'])
                            <option value="{{ $faculty['department_code'] }}" selected>{{ $faculty['department_name'] }}</option>
                            @else
                            <option value="{{ $faculty['department_code'] }}"> {{ $faculty['department_name'] }} </option>
                            @endif


                            @endforeach
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('department') }} </span>
                </div>


                <div class="col-sm-6">
                    <label for="course" class="col-sm-4 col-form-label">Course *</label>
                    <div class="col-sm-12">
                        <select class="form-control select2" id="course" name="course" style="width: 100%;">
                            <option value="" >choose your option</option>
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('course') }} </span>
                </div>

            </div>




            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="started_date" class="col-sm-4 col-form-label">Started Date *</label>
                    <input type="text" class="form-control float-right datepicker" name="started_date" value="{{ old('started_date') }}" id="datepicker">
                    <i class="far fa-calendar-alt"></i>
                    <span class="error"> {{ $errors->first('started_date') }} </span>
                </div>

            </div>

            <div class="form-group row">
                <div class="col-sm-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="apptituter" class="custom-control-input" value="1" 
                               {{ (old('apptituter') == 1 ) ? ' checked' : '' }} 
                               id="apptituter">
                               <label class="custom-control-label" for="apptituter"> Is Apptitude Test Participater ?</label>
                    </div>
                </div>
                <span class="error"> {{ $errors->first('apptituter') }} </span>
            </div>

            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

        </div>
    </form>
</div>




@endsection

@push('scripts')

<script type="text/javascript">

    $(document).ready(function () {

        var course = "{{ old('course') }}";

        //when page load is there selected facalty , load depending data
        if ($('#department').val()) {

            var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $('#department').val();

            loadDropDownData(ajax_url_faculty);

        }

        //value will change after select faculty
        $("#department").change(function (event) {

            var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $(this).val();

            loadDropDownData(ajax_url_faculty);

        });

        function loadDropDownData(ajax_url_faculty) {

            $.ajax({

                url: ajax_url_faculty,

                method: 'GET',

                success: function (data) {

                    $('#course').empty();

                    if (data == 'false') {

                        $('#course').append($("<option> choose your option </option>"));

                    } else {

                        $.each(data, function (key, value) {

                            selected = '';

                            if (key == course) {

                                selected = 'selected';
                            }

                            $('#course').append($("<option " + selected + "></option>").attr("value", key).text(value)).trigger('change.select2');
                        });
                    }
                }
            });

        }

    });

</script>

@endpush