@extends('dashboard.admin_dashboard')

@section('content')


<div class="card">
    <div class="card-header">
        <h3 class="card-title">Registered Students</h3>
        <a type="button" href="{{ Route('stu_user_form') }}" class="btn btn-block btn-primary col-sm-2 float-right">Create</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">


        <table id="stud_reg_result" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Student Reg.No.</th>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Course</th>
                    <th>Email</th>
                    <!-- <th>Action</th> -->
                </tr>
            </thead>
            <tbody>
                @foreach($getAll as $data)
                <tr>

                    <td>{{$data['reg_no']}}</td>
                    <td>{{$data['fname'].' '.$data['lname']}}</td>
                    <td>{{$data['department_name']}}</td>
                    <td>{{$data['course_name']}}</td>
                    <td>{{$data['email']}}</td>
                    <!-- <td></td> -->
                </tr>
                @endforeach


            </tbody>
            <tfoot>
                <tr>
                    <th>Student Reg.No.</th>
                    <th>Name</th>
                    <th>Department</th>
                    <th>Course</th>
                    <th>Email</th>
                    <!-- <th>Action</th> -->
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>




@endsection