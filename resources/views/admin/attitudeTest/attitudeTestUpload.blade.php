@extends('dashboard.lec_dashboard')

@section('content')

<style type="text/css">

    .error {
        color: #dc3545;
    }

</style>


<div class="card card-info">
    <div class="card-header">
        <h3 class="card-title">Upload Attitude Test Results</h3>

    </div>

    <form class="form-horizontal" method="POST" action="{{ Route('store_attituteTest_marks') }}" enctype="multipart/form-data">
        @csrf

        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="department" class="col-sm-4 col-form-label">Department *</label>
                    <div class="col-sm-12">
                        <select class="form-control select2 department" name="department" id="department" style="width: 100%;">
                            <option selected="selected" value="" >Select Option</option>
                            @foreach($form_dtls['department'] as $department)

                            @if (old('department') == $department['department_code'])
                            <option value="{{ $department['department_code'] }}" data-id="{{ $department['department_code'] }}" selected>{{ $department['department_name'] }}</option>
                            @else
                            <option value="{{ $department['department_code'] }}" data-id="{{ $department['department_code'] }}"> {{ $department['department_name'] }} </option>
                            @endif

                            @endforeach
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('department') }} </span>
                </div>

                <div class="col-sm-6">
                    <label for="course" class="col-sm-4 col-form-label">Course *</label>
                    <div class="col-sm-12">
                        <select class="form-control select2" id="course" name="course" style="width: 100%;" disabled>
                            <option value="" >choose your option</option>

                            @foreach($form_dtls['courses'] as $courses)

                            @if (old('courses') == $courses['course_code'])
                            <option value="{{ $courses['course_code'] }}" selected>{{ $courses['course_name'] }}</option>
                            @else
                            <option value="{{ $courses['course_code'] }}" class="{{ $courses['department_code'] }}"> {{ $courses['course_name'] }} </option>
                            @endif

                            @endforeach
                        </select>
                    </div>
                    <span class="error"> {{ $errors->first('course') }} </span>
                </div>
            </div>

            
            <div class="form-group row">
              <div class="col-sm-6">
                <label for="inputFile" class="col-sm-4 col-form-label">File input *</label>
                <div class="col-sm-12">
                    <input type="file"  name="inputFile" id="inputFile" value="{{ old('inputFile') }}">
                </div>
                <span class="error"> {{ $errors->first('inputFile') }} </span>
              </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
                <a type="button" href="{{ url()->previous() }}" class="btn btn-default float-right">Cancel</a>
            </div>

        </div>
    </form>
</div>

@endsection

@push('scripts')

<script type="text/javascript"> 

$(document).ready(function () {

    $(document).on('change','#department',function(){
        document.getElementById('course').disabled = false;
    });
    
    if ($('#department').val()) {
        var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $('#department').val();
        loadDropDownData(ajax_url_faculty);
    }

    $("#department").change(function (event) {
        var ajax_url_faculty = "{{ url('coursesForFaculty') }}/" + $('#department').val();
        var course =  $('#course').val();
        loadDropDownData(ajax_url_faculty);
    });

});


function loadDropDownData(ajax_url_faculty) {

            $.ajax({

                url: ajax_url_faculty,

                method: 'GET',

                success: function (data) {

                    $('#course').empty();

                    if (data == 'false') {

                        $('#course').append($("<option> choose your option </option>"));

                    } else {

                        $.each(data, function (key, value) {

                            selected = '';

                            if (key == "{{ old('course')}}") {

                                selected = 'selected';
                            }

                            $('#course').append($("<option " + selected + "></option>").attr("value", key).text(value)).trigger('change.select2');
                        });

                        var department =  $('#department').val();
                        var course     =  $('#course option:selected').val();
                        var ajax_url_course = "{{ url('modules_for_course') }}/" + department+'/'+course ;

                        loadModuleLlist(ajax_url_course);

                    }
                }
            });

        }

</script>

@endpush